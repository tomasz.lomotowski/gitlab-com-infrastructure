# Reserved IP address for container registry
locals {
  # For backwards compatibility with the gitlab-gke regional cluster
  gke_name      = var.name == "gitlab-gke" ? format("gke-%v", var.environment) : format("gke-%v-%v", var.environment, var.name)
  prom_name     = var.name == "gitlab-gke" ? format("iap-%v", var.environment) : format("iap-%v-%v", var.environment, var.name)
  prom_dns_name = var.name == "gitlab-gke" ? format("prometheus-gke.%v.gitlab.net.", var.environment) : format("prometheus-gke.%v-%v.gitlab.net", var.environment, var.name)
}

resource "google_compute_address" "registry-gke" {
  name         = "registry-${local.gke_name}"
  description  = "registry-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for git service traffic
resource "google_compute_address" "nginx-gke" {
  name         = "nginx-${local.gke_name}"
  description  = "nginx-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for prometheus operator
resource "google_compute_address" "prometheus-gke" {
  name         = "prometheus-${local.gke_name}"
  description  = "prometheus-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for ssh
resource "google_compute_address" "ssh-gke" {
  name         = "ssh-${local.gke_name}"
  description  = "ssh-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for websockets
resource "google_compute_address" "websockets-gke" {
  name         = "websockets-${local.gke_name}"
  description  = "websockets-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for Git HTTPs
# Note: We cannot call this "git" because this name is currently
# reserved for the NGINX ingress
resource "google_compute_address" "git-https-gke" {
  name         = "git-https-${local.gke_name}"
  description  = "git-https-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for prometheus ingress with IAP
resource "google_compute_global_address" "prometheus-ingress-iap" {
  name        = "prometheus-ingress-${local.prom_name}"
  description = "prometheus-ingress-${local.prom_name}"
}

module "prometheus-gke-pre-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "${local.prom_dns_name}" = {
      ttl     = "300"
      records = [google_compute_global_address.prometheus-ingress-iap.address]
    }
  }
}

# Reserved IP address for KAS
resource "google_compute_global_address" "kas-gke" {
  name        = "kas-${local.gke_name}"
  description = "kas-gke"
}
