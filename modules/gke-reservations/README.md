# GitLab.com gke-reservations Terraform Module

## What is this?

This is a generic module that is used for reserving IP addresses that are used outside of Terraform.
