
## Addresses

output "registry_gke_address" {
  value       = google_compute_address.registry-gke.address
  description = "Address of registry-gke"
}

output "prometheus_gke_address" {
  value       = google_compute_address.prometheus-gke.address
  description = "Address of prometheus-gke"
}

output "ssh_gke_address" {
  value       = google_compute_address.ssh-gke.address
  description = "Address of ssh-gke"
}

output "prometheus_ingress_iap_address" {
  value       = google_compute_global_address.prometheus-ingress-iap.address
  description = "Address of prometheus-ingress-iap"
}

output "kas_gke_address" {
  value       = google_compute_global_address.kas-gke.address
  description = "Address of kas-gke"
}
