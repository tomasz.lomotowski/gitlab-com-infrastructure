variable "project" {
  type        = string
  description = "GCP project where the bastion host will be created"
}

variable "region" {
  type        = string
  description = "GCP region where the bastion host will be created"
}

variable "environment" {
  type        = string
  description = "Name of the environment for which the bastion is being created"
}

variable "dns_zone_name" {
  type        = string
  description = "DNS zone in which the bastion FQDN is being created"
}

variable "cloudflare_account_id" {
  default = ""
}

variable "cloudflare_api_key" {
  default = ""
}

variable "cloudflare_email" {
  default = ""
}

variable "bootstrap_script_version" {
  default = 9
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_key_path = ".chef.pem"
    version       = "14.13.11"
  }
}

variable "chef_provision_bootstrap_bucket" {
  type = string
}

variable "chef_provision_bootstrap_key" {
  type = string
}

variable "chef_provision_bootstrap_keyring" {
  type = string
}

variable "chef_provision_bootstrap_username" {
  type = string
}

variable "chef_run_list" {
  type = string
}

variable "bastion_role_permissions" {
  type        = list(string)
  description = "a list of permissions that the runner managers compute service account should have"
  default = [
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list",
    "cloudkms.cryptoKeyVersions.useToDecrypt"
  ]
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "lb_fqdns_bastion" {
  type        = list(string)
  description = "List of FQDNs that the bastion server will be accessible with"
}

variable "ip_cidr_range" {
  type        = string
  description = "The IP range"
}

variable "vpc" {
  type        = string
  description = "The target network"
}

variable "machine_type" {
  type        = string
  description = "The machine size"
}
