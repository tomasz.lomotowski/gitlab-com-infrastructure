provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

resource "google_service_account" "bastion" {
  account_id   = "bastion"
  display_name = "bastion Account"
  description  = "Service account used by the bastion server. It should have minimal privileges."
}

resource "google_project_iam_custom_role" "bastion" {
  role_id     = "bastion"
  title       = "Bastion Role"
  description = "Compute service account for bastion servers"
  permissions = var.bastion_role_permissions
}

resource "google_project_iam_member" "bastion" {
  role   = google_project_iam_custom_role.bastion.id
  member = "serviceAccount:${google_service_account.bastion.email}"
}

module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version = var.bootstrap_script_version
  chef_provision = merge(
    var.chef_provision,
    {
      bootstrap_bucket  = var.chef_provision_bootstrap_bucket
      bootstrap_key     = var.chef_provision_bootstrap_key
      bootstrap_keyring = var.chef_provision_bootstrap_keyring
      user_name         = var.chef_provision_bootstrap_username
    }
  )
  chef_run_list         = var.chef_run_list
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.ip_cidr_range
  machine_type          = var.machine_type
  name                  = "bastion"
  node_count            = 1
  project               = var.project
  public_ports          = [22]
  region                = var.region
  service_account_email = google_service_account.bastion.email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = var.vpc
}
