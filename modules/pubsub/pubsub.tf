locals {
  full_topic_names = toset(formatlist(
    "%v-%v-%v-%v",
    "pubsub",
    var.pubsub_topics,
    "inf",
    var.environment,
  ))
}

resource "google_pubsub_topic" "pubsub_topics" {
  for_each = local.full_topic_names
  name     = each.value
}

resource "google_logging_project_sink" "pubsub_sinks" {
  for_each = var.pubsub_filters
  name     = "pubsub-${each.key}-inf-${var.environment}"

  destination = "pubsub.googleapis.com/projects/${var.project}/topics/pubsub-${each.key}-inf-${var.environment}"
  filter      = each.value

  # Use a unique writer (creates a unique service account used for writing)
  unique_writer_identity = true
  depends_on             = [google_pubsub_topic.pubsub_topics]
}

resource "google_service_account" "fluentd_kube" {
  account_id  = "fluentd-kube"
  description = "For use by fluentd in Kubernetes."
}

resource "google_project_iam_member" "fluentd_kube_permit_workload_identity" {
  role   = "roles/iam.workloadIdentityUser"
  member = "serviceAccount:${var.project}.svc.id.goog[logging/fluentd-elasticsearch]"
}

resource "google_project_iam_binding" "log-writer" {
  role = "roles/pubsub.publisher"

  members = concat(
    values(google_logging_project_sink.pubsub_sinks).*.writer_identity,
    [
      "serviceAccount:${var.service_account_email}",
      "serviceAccount:${google_service_account.fluentd_kube.email}"
    ],
    var.additional_pubsub_publisher_roles
  )
}

resource "google_project_iam_member" "pubsub_viewer" {
  role   = "roles/pubsub.viewer"
  member = "serviceAccount:${google_service_account.fluentd_kube.email}"
}
