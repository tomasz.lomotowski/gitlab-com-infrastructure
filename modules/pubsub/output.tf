output "topic_ids" {
  value = {
    for key, topic in google_pubsub_topic.pubsub_topics :
    key => topic.id
  }
}
