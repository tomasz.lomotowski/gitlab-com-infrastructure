variable "environment" {}

variable "project" {}

variable "pubsub_topics" {
  type    = list(string)
  default = []
}

variable "pubsub_filters" {
  type    = map(string)
  default = {}
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

variable "additional_pubsub_publisher_roles" {
  type        = list(string)
  description = "Other service accounts that should have the pubsub publisher role"
  default     = []
}
