locals {
  countries = ["SY", "KP", "CU", "IR"]
}

resource "cloudflare_filter" "country_embargo_list" {
  zone_id     = var.zone_id
  description = "Country list to block as a country on the embargo list"
  expression  = "(ip.geoip.country in {\"${join("\" \"", local.countries)}\"})"
}

resource "cloudflare_firewall_rule" "country_embargo_list" {
  zone_id     = var.zone_id
  description = "49|2020-10-02 13:37:15+00:00|long-term|Countries on Embargo list"
  filter_id   = cloudflare_filter.country_embargo_list.id
  action      = "block"
  priority    = 15049
}
