variable "secrets" {
  default     = {}
  description = "a map of secrets and users or service accounts that need access to that each secret"
}
