resource "google_secret_manager_secret" "secrets" {
  for_each = var.secrets
  labels = {
    "tfmanaged" = "yes"
  }
  project = lookup(each.value, "project", null)
  replication {
    automatic = true
  }
  secret_id = each.key
}

resource "google_secret_manager_secret_iam_binding" "secrets-access" {
  for_each   = var.secrets
  depends_on = [google_secret_manager_secret.secrets]
  members    = lookup(each.value, "readers")
  project    = lookup(each.value, "project", null)
  role       = "roles/secretmanager.secretAccessor"
  secret_id  = each.key
}
