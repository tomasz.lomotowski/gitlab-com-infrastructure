## Google Secrets Terraform Module

This terraform module creates [Google Secret Manager](https://cloud.google.com/secret-manager)
secret resources, as well as managing the permissions of which users and service accounts
have access to those secrets.

It only has one variable input, which is called `secrets`, and it takes the following format

```
secrets = {
  "secret-name" = {
    readers = [
      "service-account@my-project.iam.gserviceaccount.com",
      "another-service-account@my-project.iam.gserviceaccount.com",
    ],
    project = "my-project"
  }
}
```

The above would create a secret called "secret-name" in the project "my-project",
and grant the two service accounts the ability to read the secret. The `project`
parameter can be omitted to use the current project of the `google` provider
