resource "google_compute_firewall" "docker_machine_deny_ingress" {
  name      = "docker-machine-block-ingress"
  direction = "INGRESS"
  network   = module.vpc.network_name
  priority  = 65500

  target_tags   = ["docker-machine"]
  source_ranges = ["0.0.0.0/0"]

  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "docker_machines" {
  name      = "docker-machines"
  direction = "INGRESS"
  network   = module.vpc.network_name
  priority  = 1000

  target_tags   = ["docker-machine"]
  source_ranges = var.runner_manager_cidr

  allow {
    protocol = "tcp"
    ports    = [22, 2376]
  }
}

resource "google_compute_firewall" "deny_internal_traffic_between_runners" {
  name               = "deny-internal-traffic-between-runners"
  description        = "Deny internal traffic between runners"
  direction          = "EGRESS"
  network            = module.vpc.network_name
  priority           = 2000
  destination_ranges = ["10.0.0.0/8"]
  target_tags        = ["docker-machine"]

  deny {
    protocol = "all"
  }
}
