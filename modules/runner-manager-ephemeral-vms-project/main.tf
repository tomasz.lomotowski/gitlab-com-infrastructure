provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

##################################
#
#  Network
#
##################################

module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 2.2"

  project_id   = var.project
  network_name = var.network

  subnets = [
    {
      subnet_name   = "ephemeral-runners"
      subnet_ip     = var.ephemeral_runners_subnetwork
      subnet_region = var.region
      description   = "Subnet for all ephemeral runner machines"

      subnet_flow_logs          = "true"
      subnet_flow_logs_interval = "INTERVAL_1_MIN"
      subnet_flow_logs_sampling = 0.01
      subnet_flow_logs_metadata = "INCLUDE_ALL_METADATA"
    }
  ]
}

resource "google_compute_network_peering" "peering_ci" {
  name         = "peering-gitlab-ci"
  network      = module.vpc.network_self_link
  peer_network = var.network_ci_project
}

##################################
#
#  VPC flow logs
#
##################################

resource "google_project_iam_binding" "project" {
  project = var.project
  role    = "roles/bigquery.dataEditor"

  members = [
    google_logging_project_sink.vpc_flow_logs.writer_identity,
  ]
}

resource "google_logging_project_sink" "vpc_flow_logs" {
  name        = "vpc_flow_logs"
  destination = "bigquery.googleapis.com/projects/${var.project}/datasets/vpc_flow_logs"

  filter = <<-EOT
  resource.type="gce_subnetwork"
  logName="projects/${var.project}/logs/compute.googleapis.com%2Fvpc_flows"
  EOT

  unique_writer_identity = true

  depends_on = [
    google_bigquery_dataset.vpc_flow_logs,
  ]
}

resource "google_bigquery_dataset" "vpc_flow_logs" {
  dataset_id    = "vpc_flow_logs"
  friendly_name = "VPC Flow Logs"
  description   = "This dataset holds VPC flow logs, imported via a stackdriver sink."
  location      = "US"

  labels = {
    bq_dataset = "vpc_flow_logs"
  }

  # 14 days of retention
  default_table_expiration_ms = 1000 * 3600 * 24 * 14
}

##################################
#
#  Service Accounts
#
##################################

resource "google_project_iam_custom_role" "runner_manager" {
  role_id     = "runnerManager"
  title       = "Runner Manager Role"
  description = "Compute service account for CI runner managers"
  permissions = var.runner_manager_role_permissions
}

resource "google_service_account" "ephemeral_runner" {
  account_id   = "ephemeral-runner"
  display_name = "Ephemeral runner machine account"
  description  = "Service account assigned to the ephemeral runner machines. Should have no privileges."
}

resource "google_service_account" "stale_ci_cleaner" {
  account_id   = "stale-ci-cleaner"
  display_name = "Stale CI Runners Cleaner Account"
  description  = "Service account used by the script that cleans up stale CI runners"
  # https://ops.gitlab.net/gitlab-com/gl-infra/ci-project-cleaner
}

resource "google_project_iam_custom_role" "stale_ci_cleaner" {
  role_id     = "staleCiCleaner"
  title       = "Stale CI Runners Cleaner role"
  description = "Role for the script that cleans up stale CI runners"
  permissions = var.stale_ci_cleaner_permissions
}

resource "google_project_iam_member" "stale_ci_cleaner" {
  role   = google_project_iam_custom_role.stale_ci_cleaner.id
  member = "serviceAccount:${google_service_account.stale_ci_cleaner.email}"
}

# Developers/SRE are added to this group.
resource "google_project_iam_member" "gcp-ci-ops" {
  role   = "roles/owner"
  member = "group:gcp-ci-ops-sg@gitlab.com"
}

# Allow gitlab-ci create VMs inside of runner manager ephemeral VMs project.
resource "google_project_iam_member" "runner_manager" {
  role   = google_project_iam_custom_role.runner_manager.id
  member = "serviceAccount:${var.runner_manager_service_account_email}"
}

resource "google_project_iam_member" "runner_manager_service_account_user" {
  role   = "roles/iam.serviceAccountUser"
  member = "serviceAccount:${var.runner_manager_service_account_email}"
}
