variable "project" {
  type        = string
  description = "GCP project"
}

variable "region" {
  type        = string
  description = "Region in which the VMs will be maintained"
  default     = "us-east1"
}

##################################
#
#  Network
#
##################################

variable "network" {
  type        = string
  description = "Name of the VPC network where the VMs will be attached"
  default     = "ephemeral-runners"
}

variable "ephemeral_runners_subnetwork" {
  type        = string
  description = "CIDR of size /21 unique across all networks connected to the main one in CI project where Runner Managers are living"
}

variable "network_ci_project" {
  type    = string
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ci-155816/global/networks/default"
}

variable "runner_manager_cidr" {
  type        = list(string)
  description = "List of CIDRs that should be allowed to talk with the ephemeral-runners VMs"
}

##################################
#
#  Service Accounts
#
##################################

variable "runner_manager_service_account_email" {
  type        = string
  description = "E-mail address of the Runner Manager's service account from the gitlab-ci GCP project"
}

variable "stale_ci_cleaner_permissions" {
  type        = list(any)
  description = "A list of permissions that the stale runner script uses"
  default = [
    "compute.instances.delete",
    "compute.instances.list",
    "compute.instances.get",
  ]
}

variable "runner_manager_role_permissions" {
  type        = list(any)
  description = "A list of permissions that the runner managers compute service account should have"
  default = [
    "compute.disks.create",
    "compute.images.useReadOnly",
    "compute.instances.create",
    "compute.instances.stop",
    "compute.instances.delete",
    "compute.instances.get",
    "compute.instances.getSerialPortOutput",
    "compute.instances.setLabels",
    "compute.instances.setMetadata",
    "compute.instances.setTags",
    "compute.instances.setServiceAccount",
    "compute.subnetworks.use",
    "compute.subnetworks.useExternalIp",
    "compute.networks.updatePolicy",
    "compute.firewalls.create",
    "compute.firewalls.get",
    "compute.zoneOperations.get",
    "compute.projects.get",
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list"
  ]
}
