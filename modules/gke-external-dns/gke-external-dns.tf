# If a name is not passed to the module, exclude it for
# backwards compatibility

locals {
  dns_name   = var.name == "" ? "${var.environment}.gke.gitlab.net." : "${var.environment}-${var.name}.gke.gitlab.net."
  account_id = var.name == "" ? "kube-external-dns-${var.environment}" : "k8-ext-dns-${var.environment}-${var.name}"
  zone_name  = var.name == "" ? "kube-external-dns-${var.environment}" : "k8-ext-dns-${var.environment}-${var.name}"
}

resource "google_dns_managed_zone" "kube_external_dns" {
  name     = local.zone_name
  dns_name = local.dns_name
}

module "kube_external_dns_zone_delegation" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."
  ns = {
    "${local.dns_name}" = {
      ttl     = "3600"
      records = google_dns_managed_zone.kube_external_dns.name_servers
    }
  }
}

# Used by workload identity, no explicit private key
resource "google_service_account" "kube_external_dns" {
  account_id  = local.account_id
  description = "A service account for use by this environments's GKE cluster's external-dns deployment."
}

resource "google_project_iam_member" "kube_external_dns" {
  role   = "roles/dns.admin"
  member = "serviceAccount:${google_service_account.kube_external_dns.email}"
}

resource "google_project_iam_member" "kube_external_dns_permit_workload_identity" {
  role   = "roles/iam.workloadIdentityUser"
  member = "serviceAccount:${var.project}.svc.id.goog[external-dns/external-dns]"
}
