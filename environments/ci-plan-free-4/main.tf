terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/ci-plan-free-4/terraform.tfstate"
    region = "us-east-1"
  }
}

module "runner_manager_ephemeral_vms_project" {
  source = "../../modules/runner-manager-ephemeral-vms-project"

  project                              = var.project
  runner_manager_service_account_email = "gitlab-ci-plan-free-4@gitlab-ci-155816.iam.gserviceaccount.com"
  ephemeral_runners_subnetwork         = "10.10.24.0/21" // unique across all runner managers of all types

  runner_manager_cidr = [
    "10.142.0.5/32",
  ]
}
