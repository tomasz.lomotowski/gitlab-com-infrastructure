variable "project" {
  default = "gitlab-ci-plan-free-4-3ba81e"
}

variable "network" {
  default = "ephemeral-runners"
}
