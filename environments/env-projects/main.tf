// Configure the provider using the terraform admin project
// Reference: https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform
provider "google" {
  project = "env-zero"
  region  = "us-east1"
  version = "~> 3.44"
}

// Use Terraform Remote State backed by Google Cloud Storage
terraform {
  backend "gcs" {
    bucket = "gitlab-com-infrastructure"
    prefix = "env-projects/terraform-states"
  }
}

// Create the projects
module "gitlab-ci" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = "155816"
  project            = "gitlab-ci"
  project_id         = "gitlab-ci-155816"
  project_folder     = local.top_level_project_folder
  service_account_roles = [
    "roles/iam.serviceAccountAdmin",
    "roles/cloudkms.admin",
    "roles/iam.roleAdmin",
    "roles/resourcemanager.projectIamAdmin",
    "roles/secretmanager.admin",
  ]
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ci-windows" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = "155816"
  project            = "gitlab-ci-windows"
  project_id         = "gitlab-ci-windows"
  project_folder     = local.top_level_project_folder
  service_account_roles = [
    "roles/iam.serviceAccountAdmin",
    "roles/iam.roleAdmin",
    "roles/resourcemanager.projectIamAdmin"
  ]
}

module "gitlab-ci-macos" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.2"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account = var.billing_account
  project         = "gitlab-ci-macos"
  project_folder  = local.top_level_project_folder
  service_account_roles = [
    "roles/iam.serviceAccountAdmin",
    "roles/cloudkms.admin",
    "roles/iam.roleAdmin",
    "roles/resourcemanager.projectIamAdmin",
    "roles/secretmanager.admin",
  ]
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-org-ci" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account = var.billing_account
  project         = "gitlab-org-ci"
  project_folder  = local.top_level_project_folder
  service_account_roles = [
    "roles/iam.serviceAccountAdmin",
    "roles/cloudkms.admin",
    "roles/iam.roleAdmin",
    "roles/resourcemanager.projectIamAdmin",
    "roles/secretmanager.admin",
  ]
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ci-plan-free-3" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account       = var.billing_account
  project               = "gitlab-ci-plan-free-3"
  project_folder        = local.top_level_project_folder
  service_account_roles = local.ci.ephemeral_vms.roles
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ci-plan-free-4" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account       = var.billing_account
  project               = "gitlab-ci-plan-free-4"
  project_folder        = local.top_level_project_folder
  service_account_roles = local.ci.ephemeral_vms.roles
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ci-plan-free-5" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account       = var.billing_account
  project               = "gitlab-ci-plan-free-5"
  project_folder        = local.top_level_project_folder
  service_account_roles = local.ci.ephemeral_vms.roles
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ci-plan-free-6" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account       = var.billing_account
  project               = "gitlab-ci-plan-free-6"
  project_folder        = local.top_level_project_folder
  service_account_roles = local.ci.ephemeral_vms.roles
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ci-plan-free-7" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account       = var.billing_account
  project               = "gitlab-ci-plan-free-7"
  project_folder        = local.top_level_project_folder
  service_account_roles = local.ci.ephemeral_vms.roles
  additional_labels = {
    "environment" = "production"
  }
}

module "gitlab-ops" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ops"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-ops"
  project_folder     = local.top_level_project_folder // gitlab.com/Infrastructure
  use_name_as_id     = "true"
  service_account_roles = [
    "roles/iam.serviceAccountCreator",
    "roles/cloudfunctions.developer"
  ]
}

module "gitlab-pre" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-pre"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-pre"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
  service_account_roles = [
    "roles/cloudfunctions.developer",
    "roles/secretmanager.admin",
    "roles/cloudsql.editor"
  ]
}

module "gitlab-production" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-production"],
    ),
  )
  billing_account            = var.billing_account
  bucket_name_prefix         = ""
  project                    = "gitlab-production"
  project_folder             = local.project_folder
  serial_port_logging_enable = true
  use_name_as_id             = "true"
  additional_labels = {
    "environment" = "production"
  }
  service_account_roles = [
    "roles/cloudfunctions.developer"
  ]
}

module "gitlab-release" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-release"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-release"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}

module "gitlab-staging" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-staging"],
    ),
  )
  billing_account            = var.billing_account
  bucket_name_prefix         = ""
  project                    = "gitlab-staging"
  project_id                 = "gitlab-staging-1"
  project_folder             = local.project_folder
  serial_port_logging_enable = true
  service_account_roles = [
    "roles/cloudfunctions.developer"
  ]
}

module "gitlab-testbed" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-testbed"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-testbed"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}

module "gitlab-db-benchmarking" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-db-benchmarking"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-db-benchmarking"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
  service_account_roles = [
    "roles/cloudfunctions.developer"
  ]
}

module "gitlab-subscriptions-staging" {
  source             = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-subscriptions-staging"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}

module "gitlab-db-sharding-poc" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v8.0.3"
  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-db-sharding-poc"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-db-sharding-poc"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
  service_account_roles = [
    "roles/cloudfunctions.developer"
  ]
}
