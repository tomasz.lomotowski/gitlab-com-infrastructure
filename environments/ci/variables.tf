variable "project" {
  default = "gitlab-ci-155816"
}

variable "region" {
  default = "us-east1"
}

variable "network" {
  default = "default"
}

variable "vpc_peering_networks" {
  type        = map(string)
  description = "List of networks that are going to be peered with the environment VPC"

  default = {
    "ci-plan-free-3" = "https://www.googleapis.com/compute/v1/projects/gitlab-ci-plan-free-3-35411a/global/networks/ephemeral-runners"
    "ci-plan-free-4" = "https://www.googleapis.com/compute/v1/projects/gitlab-ci-plan-free-4-3ba81e/global/networks/ephemeral-runners"
    "ci-plan-free-5" = "https://www.googleapis.com/compute/v1/projects/gitlab-ci-plan-free-5-6ef84d/global/networks/ephemeral-runners"
    "ci-plan-free-6" = "https://www.googleapis.com/compute/v1/projects/gitlab-ci-plan-free-6-f2de7a/global/networks/ephemeral-runners"
    "ci-plan-free-7" = "https://www.googleapis.com/compute/v1/projects/gitlab-ci-plan-free-7-7fe256/global/networks/ephemeral-runners"
  }
}

# See CI variables
variable "miner_ips" {
  type    = list(string)
  default = []
}

variable "bootstrap_script_version" {
  default = 9
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-ci-chef-bootstrap"
    bootstrap_key     = "gitlab-ci-bootstrap-validation"
    bootstrap_keyring = "gitlab-ci-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "environment" {
  default = "ci"
}

variable "machine_types" {
  type = map(string)

  default = {
    "sd-exporter" = "n1-standard-1"
    "nessus"      = "n1-standard-4"
    "bastion"     = "n1-standard-1"
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "sd-exporter" = 1
    "nessus"      = 1
    "bastion"     = 1
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "sd-exporter" = []
    "nessus"      = [8834]
    "bastion"     = [22]
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-ci-155816.iam.gserviceaccount.com"
}

variable "stale_ci_cleaner_permissions" {
  type        = list(string)
  description = "A list of permissions that the stale runner script uses"
  default = [
    "compute.instances.delete",
    "compute.instances.list",
    "compute.instances.get",
  ]
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-ci-155816.iam.gserviceaccount.com"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

variable "qa_tunnel_service_account_id" {
  type    = string
  default = "qa-tunnel"
}

locals {
  ephemeral_vms_projects = toset(["3", "4", "5", "6"])
}

variable "bastion_role_permissions" {
  type        = list(string)
  description = "a list of permissions that the runner managers compute service account should have"
  default = [
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list",
    "cloudkms.cryptoKeyVersions.useToDecrypt"
  ]
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.ci.gitlab.com"]
}

variable "subnetworks" {
  type = map(string)

  default = {
    "private-runners"       = "10.0.0.0/20"
    "gitlab-shared-runners" = "10.0.16.0/20"
    "shared-runners"        = "10.0.32.0/20"
    "bastion"               = "10.1.2.0/24"
  }
}

variable "bastion_fqdns" {
  type = list(string)

  default = [
    "lb-bastion.ci.gitlab.com",
  ]
}

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}

#######################
# pubsubbeat config
#######################

variable "pubsub_topics" {
  type = list(string)
  default = [
    "system",
  ]
}

variable "pubsub_filters" {
  type = map(string)
  default = {
  }
}
