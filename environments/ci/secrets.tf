locals {

  secrets = [
    "gitlab-okta-asa_ci",
    "gitlab-omnibus-secrets_ci-155816",
    "gitlab-omnibus-secrets_ci",
    "syslog-client_ci",
  ]

  common_secret_readers = [
    "serviceAccount:terraform@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
