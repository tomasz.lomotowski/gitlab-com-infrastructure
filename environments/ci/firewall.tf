resource "google_compute_firewall" "block_miners" {
  name      = "block-miners"
  direction = "EGRESS"
  network   = var.network
  priority  = 1000

  target_tags        = ["docker-machine"]
  destination_ranges = var.miner_ips

  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "docker_machine_deny_ingress" {
  name      = "docker-machine-block-ingress"
  direction = "INGRESS"
  network   = var.network
  priority  = 65500

  target_tags   = ["docker-machine"]
  source_ranges = ["0.0.0.0/0"]

  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "docker_machines" {
  name      = "docker-machines"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  target_tags   = ["docker-machine"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = [2376]
  }
}

resource "google_compute_firewall" "prometheus" {
  name      = "prometheus"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  source_ranges = [
    "13.68.87.12/32", # gitlab.prometheus.com
    "35.227.109.92",  # gitlab-ops / prometheus-01-inf-ops
    "35.237.131.211", # gitlab-ops /prometheus-02-inf-ops
    "35.185.16.254",  # gitlab-production / prometheus-01-inf-gprd
    "34.74.136.38",   # gitlab-production / prometheus-02-inf-gprd
  ]

  allow {
    protocol = "tcp"
    ports    = [9090, 9100, 9402, 9145, 9393, 9000]
  }
}

resource "google_compute_firewall" "prometheus-internal" {
  name      = "prometheus-internal"
  direction = "INGRESS"
  network   = var.network
  priority  = 1001

  source_tags = ["prometheus-server"]
  target_tags = ["runner-managers"]

  allow {
    protocol = "tcp"
    ports    = [9100, 9402]
  }
}

resource "google_compute_firewall" "runners_cache" {
  name      = "runners-cache"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  source_ranges = ["174.138.71.155/32"]
  target_tags   = ["runners-cache"]

  allow {
    protocol = "tcp"
    ports    = [443, 1443, 5000, 9000]
  }
}

resource "google_compute_firewall" "thanos" {
  name        = "thanos"
  description = "Prometheus Thanos access"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000

  target_tags = ["prometheus-server"]

  source_ranges = [
    "35.227.109.92/32",  # prometheus-01-inf-ops
    "35.237.131.211/32", # prometheus-02-inf-ops
    "35.237.55.26/32",   # dashboards-01-inf-ops
    "35.237.254.196/32", # dashboards-com-01-inf-ops
    "104.196.117.149",   # runner-chatops-01-inf-ops
    "35.237.232.110/32", # thanos-query-01-inf-ops
    "35.231.13.200/32",  # thanos-query-02-inf-ops
    "35.196.227.20/32",  # thanos-query-03-inf-ops
  ]

  allow {
    protocol = "tcp"
    ports    = [10901, 10902]
  }
}

resource "google_compute_firewall" "deny-internal-traffic-to-managers" {
  name        = "deny-internal-traffic-to-managers"
  description = "Deny internal traffic to managers"
  direction   = "INGRESS"
  network     = var.network
  priority    = 2000

  source_ranges = ["10.0.0.0/8"]
  target_tags   = ["runner-managers"]

  deny {
    protocol = "tcp"
  }
}

resource "google_compute_firewall" "deny-internal-traffic-from-runners" {
  name               = "deny-internal-traffic-from-runners"
  description        = "Deny internal traffic from runners"
  direction          = "EGRESS"
  network            = var.network
  priority           = 2000
  destination_ranges = ["10.0.0.0/8"]
  target_tags        = ["docker-machine"]

  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "windows_autoscaled_runner" {
  name      = "windows-autoscaled-runner"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  target_tags   = ["windows-autoscaled-runner"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = [5985, 3389]
  }
}

resource "google_compute_firewall" "allow-qa-tunnel" {
  name      = "allow-qa-tunnel"
  direction = "INGRESS"
  network   = var.network
  priority  = 900

  target_tags   = ["block-ingress"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = [443]
  }
}
