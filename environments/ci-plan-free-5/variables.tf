variable "project" {
  default = "gitlab-ci-plan-free-5-6ef84d"
}

variable "network" {
  default = "ephemeral-runners"
}
