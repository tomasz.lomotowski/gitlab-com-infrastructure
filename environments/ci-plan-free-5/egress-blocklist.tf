
module "egress-blocklist" {
  # Intentionally not verion pinned. See
  # https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/egress-blocklist#noteworthy-details
  # tflint-ignore: terraform_module_pinned_source
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/egress-blocklist.git?ref=master"
  ip_set      = "ci-free"
  gcp_project = var.project
  gcp_network = var.network
  description = "Managed via https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/egress-blocklist"
}
