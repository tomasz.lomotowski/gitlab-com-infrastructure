###########################################################
# This is specific to the pre environment
# and defines the mapping from monitoring hosts to backend
# services

resource "google_compute_url_map" "monitoring-lb" {
  name            = format("%v-monitoring-lb", var.environment)
  default_service = module.prometheus.google_compute_backend_service_self_link[0]

  host_rule {
    hosts        = ["prometheus.pre.gitlab.net"]
    path_matcher = "prometheus"
  }

  path_matcher {
    name            = "prometheus"
    default_service = module.prometheus.google_compute_backend_service_self_link[0]

    path_rule {
      paths   = ["/*"]
      service = module.prometheus.google_compute_backend_service_self_link[0]
    }
  }

  ###################################

  host_rule {
    hosts        = ["prometheus-app.pre.gitlab.net"]
    path_matcher = "prometheus-app"
  }
  path_matcher {
    name            = "prometheus-app"
    default_service = module.prometheus-app.google_compute_backend_service_self_link[0]

    path_rule {
      paths   = ["/*"]
      service = module.prometheus-app.google_compute_backend_service_self_link[0]
    }
  }
}

