locals {

  secrets = [
    "bastion_pre",
    "frontend-loadbalancer_pre",
    "gitlab-consul_pre-client",
    "gitlab-consul_pre-cluster",
    "gitlab-elk_pre",
    "gitlab-omnibus-secrets_pre",
    "gitlab-prometheus-secrets_pre",
    "gitlab-proxy_pre",
    "imap-mailbox-exporter_pre",
    "syslog-client_pre",
  ]

  common_secret_readers = [
    "serviceAccount:terraform@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
