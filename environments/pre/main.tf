## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/pre/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

## Local
provider "local" {
  version = "~> 1.4.0"
}

variable "gitlab_com_zone_id" {
}

variable "gitlab_net_zone_id" {
}

## Google

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "google-beta" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

##################################
#
#  Network
#
#################################

module "network" {
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  project          = var.project
  environment      = var.environment
  internal_subnets = var.internal_subnets
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = length(var.peer_networks["names"])
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = var.network_env
  peer_network = element(var.peer_networks["links"], count.index)
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "gitlab.net"
  dns_zone_prefix = "pre."
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  bootstrap_version = 6
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size    = 50
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  public_ports          = ["22"]
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "prometheus",
      "port" : element(
        var.monitoring_hosts["ports"],
        index(var.monitoring_hosts["names"], "prometheus"),
      ),
      "health_check_path" : "/graph",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "prometheus-app" {
  bootstrap_version = 6
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size    = 50
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus-app"
  node_count            = var.node_count["prometheus-app"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "prometheus-app",
      "port" : element(
        var.monitoring_hosts["ports"],
        index(var.monitoring_hosts["names"], "prometheus-app"),
      ),
      "health_check_path" : "/graph",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = 6
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

###############################################
#
# Load balancer and VM for the pre bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = 6
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Console
#
##################################

module "console" {
  assign_public_ip      = true
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-console-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.console_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["console"]
  machine_type          = var.machine_types["console"]
  name                  = "console"
  node_count            = var.node_count["console"]
  os_disk_size          = 50
  project               = var.project
  public_ports          = var.public_ports["console"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Consul
#
##################################

module "consul" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["consul"]
  machine_type          = var.machine_types["consul"]
  name                  = "consul"
  node_count            = var.node_count["consul"]
  project               = var.project
  public_ports          = var.public_ports["consul"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 8300
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.15.0"
  secrets_viewer_access = [
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}",
    "serviceAccount:${google_service_account.k8s-workloads.email}"
  ]
  project = var.project
}

##################################
#
#  PubSub
#
##################################

module "pubsub" {
  source                = "../../modules/pubsub"
  environment           = var.environment
  project               = var.project
  pubsub_topics         = var.pubsub_topics
  pubsub_filters        = var.pubsub_filters
  service_account_email = var.service_account_email
}

##################################
#
#  Pubsubbeat for GKE
#
##################################

## GKE Workload Identity config for beats in kubernetes

# Create a GCP service account
resource "google_service_account" "pubsubbeat-k8s-sa" {
  account_id   = "pubsubbeat-k8s"
  display_name = "pubsubbeat-k8s"
  description  = "Service account used by pubsubbeat in k8s for subscribing to PubSub"
}

# Bind a k8s service account pubsubbeat/es-diagnostics to a GCP service account
resource "google_service_account_iam_member" "pubsubbeat-pubsubbeat" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.pubsubbeat-k8s-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[pubsubbeat/pubsubbeat]"
}

# Give the GCP service account relevant PubSub permissions (assign roles)
resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-subscriber" {
  project = var.project
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

##################################
#
#  External HAProxy LoadBalancer
#
##################################

module "fe-lb" {
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-fe]\""
  create_backend_service = false
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe"
  node_count             = var.node_count["fe-lb"]
  os_boot_image          = "ubuntu-os-cloud/ubuntu-1804-lts"
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Pages
#
##################################

module "fe-lb-pages" {
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-pages]\""
  create_backend_service = false
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-pages"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe-pages"
  node_count             = var.node_count["fe-lb-pages"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_port           = 7331
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Registry
#
##################################

module "fe-lb-registry" {
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-registry]\""
  create_backend_service = false
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-registry"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe-registry"
  node_count             = var.node_count["fe-lb-registry"]
  project                = var.project
  os_boot_image          = "ubuntu-os-cloud/ubuntu-1804-lts"
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  GCP TCP LoadBalancers
#
##################################

#### Load balancer for the main site
module "gcp-tcp-lb" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs["names"])
  name                   = "gcp-tcp-lb"
  names                  = var.tcp_lbs["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe"]
}

#### Load balancer for pages
module "gcp-tcp-lb-pages" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_pages["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_pages
  gitlab_zone            = "gitlab.io."
  health_check_ports     = var.tcp_lbs_pages["health_check_ports"]
  instances              = module.fe-lb-pages.instances_self_link
  lb_count               = length(var.tcp_lbs_pages["names"])
  name                   = "gcp-tcp-lb-pages"
  names                  = var.tcp_lbs_pages["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe-pages"]
}

#### Load balancer for registry
module "gcp-tcp-lb-registry" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_registry["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_registry
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_registry["health_check_ports"]
  instances              = module.fe-lb-registry.instances_self_link
  lb_count               = length(var.tcp_lbs_registry["names"])
  name                   = "gcp-tcp-lb-registry"
  names                  = var.tcp_lbs_registry["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe-registry"]
}

##################################
#
#  Deploy
#
##################################

module "deploy" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-deploy-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.deploy_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["deploy"]
  machine_type          = var.machine_types["deploy"]
  name                  = "deploy"
  node_count            = var.node_count["deploy"]
  project               = var.project
  public_ports          = var.public_ports["deploy"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Web front-end
#
#################################

module "web" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web"]
  machine_type          = var.machine_types["web"]
  name                  = "web"
  node_count            = var.node_count["web"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Pages web front-end
#
#################################

module "web-pages" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web-pages]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web-pages"]
  machine_type          = var.machine_types["web-pages"]
  name                  = "web-pages"
  node_count            = var.node_count["web-pages"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web-pages"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Sidekiq
#
##################################

module "sidekiq" {
  bootstrap_version                           = var.bootstrap_script_version
  chef_provision                              = var.chef_provision
  chef_run_list                               = "\"role[${var.environment}-base-be-sidekiq-catchall]\""
  dns_zone_name                               = var.dns_zone_name
  environment                                 = var.environment
  ip_cidr_range                               = var.subnetworks["sidekiq"]
  machine_type                                = var.machine_types["sidekiq-catchall"]
  name                                        = "sidekiq"
  os_disk_type                                = "pd-ssd"
  project                                     = var.project
  public_ports                                = var.public_ports["sidekiq"]
  region                                      = var.region
  service_account_email                       = var.service_account_email
  sidekiq_elasticsearch_count                 = "0"
  sidekiq_elasticsearch_instance_type         = "none"
  sidekiq_low_urgency_cpu_bound_count         = "0"
  sidekiq_low_urgency_cpu_bound_instance_type = "none"
  sidekiq_catchall_count                      = var.node_count["sidekiq-catchall"]
  sidekiq_catchall_instance_type              = var.machine_types["sidekiq-catchall"]
  sidekiq_urgent_other_count                  = "0"
  sidekiq_urgent_other_instance_type          = "none"
  sidekiq_urgent_cpu_bound_count              = "0"
  sidekiq_urgent_cpu_bound_instance_type      = "none"
  source                                      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-sidekiq.git?ref=v7.1.0"
  tier                                        = "sv"
  use_new_node_name                           = true
  vpc                                         = module.network.self_link
}

##################################
#
#  Gitaly Praefect nodes
#
##################################

module "praefect" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-be-praefect]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  health_check_port     = 9652
  ip_cidr_range         = var.subnetworks["praefect"]
  machine_type          = var.machine_types["praefect"]
  name                  = "praefect"
  node_count            = var.node_count["praefect"]
  project               = var.project
  public_ports          = var.public_ports["praefect"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "praefect-db" {
  availability_type = "REGIONAL" # REGIONAL enables HA
  database_version  = "POSTGRES_11"
  name              = "praefect-db"
  project           = var.project
  region            = var.region
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.3.1"
  tier              = var.machine_types["praefect-db"]
  # Note: Network peering can be moody. If your `apply` fails to create the
  # `private_vpc_connection` you can try resetting it:
  # gcloud services vpc-peerings update --service=servicenetworking.googleapis.com --ranges=private-ip --network=pre --project=gitlab-pre --force
  # See https://github.com/terraform-providers/terraform-provider-google/issues/3294#issuecomment-476715149
  vpc = module.network.self_link

  ip_configuration = {
    private_network = module.network.self_link
  }
}

module "preprod-db" {
  database_version = "POSTGRES_11"
  disk_autoresize  = true
  name             = "gitlab-pre"
  project          = var.project
  region           = var.region
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.3.1"
  tier             = "db-custom-1-3840"
  vpc              = module.network.self_link

  backup_configuration = {
    enabled    = true
    start_time = "12:00"
  }

  ip_configuration = {
    private_network = module.network.self_link
  }

  maintenance_window_day          = 7
  maintenance_window_hour         = 0
  maintenance_window_update_track = "stable"
}

module "preprod-db-postgresql-12" {
  database_version = "POSTGRES_12"
  disk_autoresize  = true
  name             = "gitlab-pre-pg-12"
  project          = var.project
  region           = var.region
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.1.8"
  tier             = "db-custom-1-3840"
  vpc              = module.network.self_link

  backup_configuration = {
    enabled    = true
    start_time = "12:00"
  }

  ip_configuration = {
    private_network = module.network.self_link
  }

  maintenance_window_day          = 7
  maintenance_window_hour         = 0
  maintenance_window_update_track = "stable"
}

module "registry-db" {
  availability_type = "REGIONAL" # REGIONAL enables HA
  database_version  = "POSTGRES_12"
  name              = "registry-db"
  project           = var.project
  region            = var.region
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.3.1"
  tier              = var.machine_types["registry-db"]
  db_name           = "registry_production"
  user_name         = "registry"

  # Note: Network peering can be moody. If your `apply` fails to create the
  # `private_vpc_connection` you can try resetting it:
  # gcloud services vpc-peerings update --service=servicenetworking.googleapis.com --ranges=private-ip --network=pre --project=gitlab-pre --force
  # See https://github.com/terraform-providers/terraform-provider-google/issues/3294#issuecomment-476715149
  vpc = module.network.self_link

  ip_configuration = {
    private_network = module.network.self_link
  }

  # count of read replicas
  read_replica_size = 1

  read_replica_disk_autoresize = false
  read_replica_zones           = "c"
  read_replica_tier            = var.machine_types["registry-db"]
  read_replica_ip_configuration = {
    private_network = module.network.self_link
  }
}

##################################
#
#  Gitaly node
#
##################################

module "gitaly" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-be-gitaly]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["gitaly"]
  machine_type          = var.machine_types["gitaly"]
  name                  = "gitaly"
  node_count            = var.node_count["gitaly"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["gitaly"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "thanos-compact" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-compact]\""
  data_disk_size            = 200
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-compact"]
  machine_type              = var.machine_types["thanos"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-compact"
  node_count                = var.node_count["thanos"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.3"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

############################
# Stackdriver log exclusions
############################

module "stackdriver" {
  sd_log_filters = var.sd_log_filters
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/stackdriver.git?ref=v2.0.0"
}
