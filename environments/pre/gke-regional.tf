##################################
#
#  GKE Cluster for pre.gitlab.com GitLab services
#
##################################

# Warning: for changes to the external DNS module you will need to run:
# `tf apply -target module.gke-external-dns.google_dns_managed_zone.kube_external_dns` first!
# See https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/merge_requests/1967


# Service account utilized by CI
resource "google_service_account" "k8s-workloads" {
  account_id   = "k8s-workloads"
  display_name = "k8s-workloads"
}

# Service account utilized by CI for Read Only operations
resource "google_service_account" "k8s-workloads-ro" {
  account_id   = "k8s-workloads-ro"
  display_name = "k8s-workloads-ro"
}

resource "google_project_iam_custom_role" "k8s-workloads" {
  project     = var.project
  role_id     = "k8sWorkloads"
  title       = "k8s-workloads"
  permissions = ["clientauthconfig.clients.listWithSecrets", "container.secrets.list"]
}

resource "google_project_iam_binding" "k8s-workloads" {
  project = var.project
  role    = "projects/${var.project}/roles/${google_project_iam_custom_role.k8s-workloads.role_id}"

  members = [
    "serviceAccount:${google_service_account.k8s-workloads.email}",
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}"
  ]
}

# IP address for NAT
resource "google_compute_address" "gke-cloud-nat-ip" {
  name        = "gitlab-gke"
  description = "gitlab-gke"
}

# In this module contains IP reservations for the GitLab environment
module "gke-reservations" {
  name                     = "gitlab-gke"
  source                   = "../../modules/gke-reservations"
  gke_subnetwork_self_link = module.gitlab-gke.subnetwork_self_link
  environment              = var.environment
}

# Reserved IP address for plantuml, must be global

resource "google_compute_global_address" "plantuml-gke" {
  name        = "plantuml-gke-${var.environment}"
  description = "plantuml-gke-${var.environment}"
}

module "pre-plantuml-gitlab-static-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab-static.net."

  a = {
    "pre.plantuml.gitlab-static.net." = {
      ttl     = "300"
      records = [google_compute_global_address.plantuml-gke.address]
    }
  }
}

# Reserved IP address for jaeger

resource "google_compute_global_address" "jaeger-iap" {
  name        = "jaeger-iap-${var.environment}"
  description = "jaeger-iap-${var.environment}"
}

module "jaeger-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "jaeger.pre.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.jaeger-iap.address]
    }
  }
}

resource "google_compute_router" "nat-router" {
  name    = "gitlab-gke"
  network = module.network.self_link
}

resource "google_compute_router_nat" "gke-nat" {
  name                               = "gitlab-gke"
  router                             = google_compute_router.nat-router.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.gke-cloud-nat-ip.self_link]
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = module.gitlab-gke.subnetwork_self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

module "gitlab-gke" {
  environment                = var.environment
  name                       = "gitlab-gke"
  vpc                        = module.network.self_link
  source                     = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v11.3.0"
  ip_cidr_range              = var.subnetworks["gitlab-gke"]
  disable_network_policy     = "false"
  dns_zone_name              = var.dns_zone_name
  kubernetes_version         = "1.18"
  nodes_service_account_name = "gitlab-gke-nodes"
  maintenance_policy = {
    start_time = "2020-09-14T02:00:00Z"
    end_time   = "2020-09-14T08:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=MO,TU"
  }
  private_master_cidr        = var.master_cidr_subnets["gitlab-gke"]
  project                    = var.project
  region                     = var.region
  release_channel            = "REGULAR"
  pod_ip_cidr_range          = var.subnetworks["gitlab-gke-pod-cidr"]
  service_ip_cidr_range      = var.subnetworks["gitlab-gke-service-cidr"]
  subnet_nat_name            = "gitlab-gke-pre"
  upgrade_notification_queue = google_pubsub_topic.gke_notifications.id
  workload_identity_ksa = {
    "gitlab/gitlab-webservice" = ["roles/cloudprofiler.agent"],
    "gitlab/gitlab-registry"   = ["roles/cloudprofiler.agent"]
  }

  node_pools = {
    "default-1" = {
      machine_type      = var.machine_types["gitlab-gke"]
      max_node_count    = var.gke_nodepool_max_nodes["default-regional"]
      node_disk_size_gb = "50"
      node_disk_type    = "pd-standard"
      node_auto_upgrade = true
      preemptible       = "true"
      type              = "default"
    }
  }
}

module "gke-external-dns" {
  source = "../../modules/gke-external-dns"

  environment = var.environment
  project     = var.project
}

// Allow GKE to talk to the prometheus operator which utilizes port 8443
resource "google_compute_firewall" "gke-master-to-kubelet" {
  name    = "k8s-api-to-kubelets"
  network = module.network.self_link
  project = var.project

  description = "GKE API to kubelets"

  source_ranges = [
    var.master_cidr_subnets["gitlab-gke"],
  ]

  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }

  target_tags = ["gitlab-gke"]
}

// Pubsub topic for gke-notificationsj
resource "google_pubsub_topic" "gke_notifications" {
  name = "gke_notifications"
}

// Cloud function to get upgrade notifications/annotations from gke
resource "google_cloudfunctions_function" "gke_notifications" {
  entry_point = "HelloPubSub"
  environment_variables = {
    ENVIRONMENT     = "ops",
    GRAFANA_API_KEY = var.grafana_api_key,
    GRAFANA_URL     = "https://dashboards.gitlab.net/api/annotations",
  }
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.gke_notifications.id
  }
  name                  = "gke_notifications"
  project               = var.project
  region                = var.region
  runtime               = "go113"
  source_archive_bucket = "gitlab-gke-notifications-function"
  source_archive_object = "gke-notifications.zip"
}

// DNS record for KAS
module "kas-pre-gitlab-com-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.com."

  a = {
    "kas.pre.gitlab.com." = {
      ttl     = "300"
      records = [module.gke-reservations.kas_gke_address]
    }
  }
}

// Reserved IP for KAS internal ingress
resource "google_compute_address" "kas-internal-gke" {
  name         = "kas-internal-gke-pre"
  description  = "kas-internal-gke-pre"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

// DNS record for KAS Internal
module "kas-int-pre-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "kas.int.pre.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_address.kas-internal-gke.address]
    }
  }
}

// Cloud Armor policy for kas.pre.gitlab.net
resource "google_compute_security_policy" "kas-ingress-policy" {
  name = "kas-ingress-policy"
  rule {
    action   = "allow"
    priority = "2147483647"
    match {
      versioned_expr = "SRC_IPS_V1"
      config {
        src_ip_ranges = ["*"]
      }
    }
    description = "default rule allow all IPs"
  }
}
