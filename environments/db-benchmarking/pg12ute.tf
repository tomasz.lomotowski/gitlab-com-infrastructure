# Postgres 12 Upgrade Testing Environment

#############################################
#
#  Patroni upgrade source and target
#
#############################################

module "pg12ute-patroni-source" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pg12ute-patroni-source]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["pg12ute-patroni-source"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["pg12ute-patroni-source"]
  machine_type           = var.machine_types["patroni"]
  name                   = "pg12ute-patroni-source"
  node_count             = var.node_count["pg12ute-patroni-source"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v3.2.2"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}


module "pg12ute-patroni-target" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pg12ute-patroni-target]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["pg12ute-patroni-target"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["pg12ute-patroni-target"]
  machine_type           = var.machine_types["pg12ute-patroni-target"]
  name                   = "pg12ute-patroni-target"
  node_count             = var.node_count["pg12ute-patroni-target"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v3.2.2"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}
