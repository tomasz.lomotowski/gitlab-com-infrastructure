variable "oauth2_client_id_monitoring" {
}

variable "oauth2_client_secret_monitoring" {
}

variable "gitlab_net_zone_id" {}

variable "domain" {
  default = "gitlab.com"
}

variable "environment" {
  default = "db-benchmarking"
}
variable "project" {
  default = "gitlab-db-benchmarking"
}

variable "region" {
  default = "us-east1"
}

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}
variable "cloudflare_zone_name" {}
variable "cloudflare_zone_id" {}

variable "bootstrap_script_version" {
  default = 8
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-db-benchmarking-chef-bootstrap"
    bootstrap_key     = "gitlab-db-benchmarking-bootstrap-validation"
    bootstrap_keyring = "gitlab-db-benchmarking-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "service_account_email" {
  type    = string
  default = "terraform@gitlab-db-benchmarking.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-db-benchmarking.iam.gserviceaccount.com"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

variable "internal_subnets" {
  type    = list(string)
  default = ["10.255.0.0/16"]
}

variable "other_monitoring_subnets" {
  type = list(string)

  default = []
}

variable "subnetworks" {
  type = map(string)

  default = {
    "bastion"                = "10.255.1.0/24"
    "console"                = "10.255.11.0/24"
    "consul"                 = "10.255.3.0/24"
    "jmeter"                 = "10.255.5.0/24"
    "monitoring"             = "10.255.6.0/24"
    "patroni"                = "10.255.12.0/24"
    "patroni-zfs"            = "10.255.2.0/24"
    "patroni-registry"       = "10.255.13.0/24"
    "patroni-zfs-registry"   = "10.255.14.0/24"
    "pgbouncer"              = "10.255.4.0/24"
    "pgbouncer-registry"     = "10.255.15.0/24"
    "pgbouncer-sidekiq"      = "10.255.7.0/24"
    "pg12ute-patroni-source" = "10.255.8.0/24"
    "pg12ute-patroni-target" = "10.255.9.0/24"
  }
}

variable "egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

variable "console_egress_ports" {
  type    = list(string)
  default = ["80", "443", "9243", "22"]
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.db-benchmarking.gitlab.com"]
}

variable "data_disk_sizes" {
  type = map(string)

  default = {
    "patroni"                = "16000"
    "patroni-registry"       = "1000"
    "pg12ute-patroni-source" = "16000"
    "pg12ute-patroni-target" = "16000"
    "prometheus"             = "4000"
  }
}

variable "machine_types" {
  type = map(string)

  default = {
    "bastion"                = "g1-small"
    "console"                = "n1-standard-4"
    "consul"                 = "g1-small"
    "jmeter"                 = "n1-standard-32"
    "monitoring"             = "n1-standard-2"
    "patroni"                = "n1-highmem-96"
    "patroni-registry"       = "n1-standard-4"
    "pgbouncer"              = "n1-standard-4"
    "pgbouncer-registry"     = "n1-standard-4"
    "pg12ute-patroni-target" = "n1-highmem-96"
    "sd-exporter"            = "n1-standard-1"
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "bastion"                = 1
    "console"                = 1
    "consul"                 = 1
    "jmeter"                 = 2
    "patroni"                = 1
    "patroni-zfs"            = 1
    "patroni-registry"       = 2
    "patroni-zfs-registry"   = 1
    "pgbouncer"              = 3
    "pgbouncer-registry"     = 3
    "pgbouncer-sidekiq"      = 3
    "pg12ute-patroni-source" = 3
    "pg12ute-patroni-target" = 3
    "prometheus"             = 1
    "sd-exporter"            = 1
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "bastion"     = [22]
    "console"     = []
    "consul"      = []
    "jmeter"      = []
    "patroni"     = []
    "pgbouncer"   = []
    "sd-exporter" = []
  }
}

variable "monitoring_hosts" {
  type = map(list(string))

  default = {
    "names" = ["prometheus"]
    "ports" = [9090]
  }
}

#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = map(list(string))

  default = {
    "subnets" = []
    "ports"   = []
  }
}

#######################
# pubsubbeat config
#######################

variable "pubsub_topics" {
  type = list(string)
  default = [
    "postgres",
    "system",
  ]
}

variable "pubsub_filters" {
  type = map(string)
  default = {
  }
}

variable "pubsubbeat-k8s-sa" {
  type = map(string)

  default = {
    "project" = "gitlab-production"
    "email"   = "pubsubbeat-k8s@gitlab-production.iam.gserviceaccount.com"
  }
}

variable "k8s-workloads-ro-sa" {
  type = map(string)

  default = {
    "project" = "gitlab-production"
    "email"   = "k8s-workloads-ro@gitlab-production.iam.gserviceaccount.com"
  }
}
