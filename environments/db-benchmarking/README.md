# db-benchmarking

## What is this?

A terraform environment for the Database Benchmarking environment. Its purpose
is to serve as a testbed for Database benchmarking and testing using
production-like data.

## Limitations

Given production data is used in this environment, access and allowed
applications are limited. **No applications that require outoging non-anonymized
traffic are allowed** (for example, no GitLab instances).
