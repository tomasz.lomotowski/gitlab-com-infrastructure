
# Cloudflare Page Rules for gitlab.com
#
# Runbook: TODO
#
# If you want these rules to be properly ordered, you will need to specify
# a "depends_on" argument for the resource that refers to the next lowest
# priority rule. This is because the provider adds rules, and their priority
# is set on how the provider adds them, and not what is set in the priority
# argument.
#
# In theory, when an apply is made, the first rule updated is always given
# a priority of "1" (the lowest). Using depends_on should force the proper
# order.
#

resource "cloudflare_page_rule" "email_customers_gitlab_com_page_rule" {
  zone_id    = var.cloudflare_zone_id
  target     = "email.customers.${var.cloudflare_zone_name}/*"
  depends_on = [cloudflare_page_rule.crk_mythical_gitlab_com_page_rule]
  priority   = 10
  status     = "active"

  actions {
    ssl = "flexible"
  }
}

resource "cloudflare_page_rule" "crk_mythical_gitlab_com_page_rule" {
  zone_id    = var.cloudflare_zone_id
  target     = "https://${var.cloudflare_zone_name}/crk-mythical/andrax-hackers-platform-v5*raw/master/*"
  depends_on = [cloudflare_page_rule.gitlab_com_deprecated_2_archive_page_rule]
  priority   = 9
  status     = "active"

  actions {
    disable_security            = true
    browser_check               = "off"
    browser_cache_ttl           = "2678400"
    response_buffering          = "off"
    cache_level                 = "cache_everything"
    edge_cache_ttl              = "2678400"
    sort_query_string_for_cache = "on"
    respect_strong_etag         = "on"
    cache_deception_armor       = "off"
    explicit_cache_control      = "off"
    disable_apps                = true
    disable_performance         = true
  }
}

# We want to cache archive objects, even if they are accessed via the API.
resource "cloudflare_page_rule" "gitlab_com_deprecated_2_archive_page_rule" {
  zone_id    = var.cloudflare_zone_id
  target     = "${var.cloudflare_zone_name}/*/repository/*/archive.*"
  depends_on = [cloudflare_page_rule.gitlab_com_deprecated_1_archive_page_rule]
  priority   = 8
  status     = "active"

  actions {
    cache_level = "cache_everything"
  }
}

# We want to cache archive objects, even if they are accessed via the API.
resource "cloudflare_page_rule" "gitlab_com_deprecated_1_archive_page_rule" {
  zone_id    = var.cloudflare_zone_id
  target     = "${var.cloudflare_zone_name}/*/repository/archive.*"
  depends_on = [cloudflare_page_rule.gitlab_com_raw_page_rule]
  priority   = 7
  status     = "active"

  actions {
    cache_level = "cache_everything"
  }
}

# We want to cache RAW requests, even if they are accessed via the API.
resource "cloudflare_page_rule" "gitlab_com_raw_page_rule" {
  zone_id    = var.cloudflare_zone_id
  target     = "${var.cloudflare_zone_name}/*/raw/*"
  depends_on = [cloudflare_page_rule.gitlab_com_archive_page_rule]
  priority   = 6
  status     = "active"

  actions {
    cache_level = "cache_everything"
  }
}

# We want to cache archive objects, even if they are accessed via the API.
resource "cloudflare_page_rule" "gitlab_com_archive_page_rule" {
  zone_id    = var.cloudflare_zone_id
  target     = "${var.cloudflare_zone_name}/*/-/archive/*"
  depends_on = [cloudflare_page_rule.gitlab_com_api_page_rule]
  priority   = 5
  status     = "active"

  actions {
    cache_level = "cache_everything"
  }
}

# Default API rule. Refer to the documentation in the URL here:
# https://support.cloudflare.com/hc/en-us/articles/200504045-Using-Cloudflare-with-your-API
resource "cloudflare_page_rule" "gitlab_com_api_page_rule" {
  zone_id    = var.cloudflare_zone_id
  target     = "${var.cloudflare_zone_name}/api/v4/*"
  depends_on = [cloudflare_page_rule.gitlab_com_jwt_auth_security]
  priority   = 4
  status     = "active"

  actions {
    browser_check  = "off"
    waf            = "off"
    always_online  = "off"
    security_level = "off"
    cache_level    = "bypass"
  }
}

# jwt auth turn off security
resource "cloudflare_page_rule" "gitlab_com_jwt_auth_security" {
  zone_id    = var.cloudflare_zone_id
  target     = "${var.cloudflare_zone_name}/jwt/auth*"
  depends_on = [cloudflare_page_rule.gitlab_com_oauth_security]
  priority   = 3
  status     = "active"

  actions {
    security_level = "off"
  }
}

# oach turn off security
resource "cloudflare_page_rule" "gitlab_com_oauth_security" {
  zone_id    = var.cloudflare_zone_id
  target     = "${var.cloudflare_zone_name}/oauth/*"
  depends_on = [cloudflare_page_rule.gitlab_com_under_attack]
  priority   = 2
  status     = "active"

  actions {
    security_level = "off"
  }
}

# I'm under attack fire-break rule.
# This rule should ALWAYS be the lowest priority. This rule exists to be turned
# on only when we want rules above this to keep workinb, but we suspect non-api
# abuse and want to block it. By default, this rule should always be off.
resource "cloudflare_page_rule" "gitlab_com_under_attack" {
  zone_id  = var.cloudflare_zone_id
  target   = "${var.cloudflare_zone_name}/*"
  priority = 1
  status   = "disabled"

  actions {
    security_level = "under_attack"
  }
}
