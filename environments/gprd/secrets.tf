locals {

  secrets = [
    "bastion_gprd",
    "blackbox-exporter_gprd",
    "camoproxy_gprd",
    "contributors_gprd",
    "cookbook-gitlab-runner_gprd",
    "elasticsearch-exporter_gprd",
    "frontend-loadbalancer_gprd",
    "gitlab-consul_gprd-client",
    "gitlab-consul_gprd-cluster",
    "gitlab-elk_gprd-duplicate",
    "gitlab-migration_gprd",
    "gitlab-monitor_gprd",
    "gitlab-oauth2-proxy-prometheus-2_gitlab-oauth2-proxy-prometheus-2-gprd",
    "gitlab-omnibus-secrets_gprd",
    "gitlab-patroni_gprd",
    "gitlab-pgbouncer_gprd",
    "gitlab-prometheus-secrets_gprd",
    "gitlab-restore-secrets_postgres-gprd",
    "gitlab-wale_gprd",
    "gitlab-walg_gprd",
    "imap-mailbox-exporter_gprd",
    "pgbouncer_gprd",
    "postgres-exporter_gprd",
    "praefect-omnibus-secrets_gprd",
    "prometheus-alertmanager_gprd",
    "redis_analysis_gprd",
    "syslog-client_gprd",
    "tstvault_gprd",
    "version-gitlab-com_gprd",
  ]

  common_secret_readers = [
    "serviceAccount:terraform@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
