# GCP Reservations
#
# Reference:
# * Terraform: https://www.terraform.io/docs/providers/google/r/compute_reservation.html
# * GCP: https://cloud.google.com/compute/docs/instances/reserving-zonal-resources

# us-east1-b
resource "google_compute_reservation" "c2-4_us-east1-b" {
  name = "c2-4-us-east1-b"
  zone = "us-east1-b"

  specific_reservation {
    count = 10
    instance_properties {
      machine_type = "c2-standard-4"
    }
  }
}

# us-east1-c
resource "google_compute_reservation" "c2-4_us-east1-c" {
  name = "c2-4-us-east1-c"
  zone = "us-east1-c"

  specific_reservation {
    count = 10
    instance_properties {
      machine_type = "c2-standard-4"
    }
  }
}

# us-east1-d
resource "google_compute_reservation" "c2-4_us-east1-d" {
  name = "c2-4-us-east1-d"
  zone = "us-east1-d"

  specific_reservation {
    count = 10
    instance_properties {
      machine_type = "c2-standard-4"
    }
  }
}
