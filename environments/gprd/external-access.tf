##################################
#
#  DR
#
#################################

resource "google_compute_firewall" "allow-dr-postgres" {
  name        = "allow-dr-postgres"
  description = "Allows postgres traffic from our DR environment into gprd"
  network     = var.environment

  allow {
    protocol = "tcp"

    ports = [
      "5432",
    ]
  }

  source_ranges = [
    "10.251.9.0/24",
  ]

  target_tags = [
    "patroni",
  ]
}

resource "google_service_account" "dr-sa" {
  account_id = "disaster-recovery"
}

##################################
#
#  gitlab-analysis
#
#################################

resource "google_compute_network_peering" "peering-gitlab-analysis-default" {
  name         = "peering-gitlab-analysis-default"
  network      = var.network_env
  peer_network = "https://www.googleapis.com/compute/v1/projects/gitlab-analysis/global/networks/default"
}

resource "google_compute_network_peering" "peering-gitlab-analysis-gitlab-analysis-vpc" {
  name         = "peering-gitlab-analysis-gitlab-analysis-vpc"
  network      = var.network_env
  peer_network = "https://www.googleapis.com/compute/v1/projects/gitlab-analysis/global/networks/gitlab-analysis-vpc"
}

resource "google_compute_firewall" "allow-postgres-gitlab-analysis-default" {
  name        = "allow-postgres-gitlab-analysis-default"
  description = "allow gitlab-analysis network default to access gprd network"
  network     = var.network_env

  source_ranges = [
    "10.52.0.0/14", # only from us-west1 default subnet
  ]

  target_tags = [
    "postgres-dr-archive",
    "patroni-v12-zfs",
  ]

  allow {
    protocol = "tcp"
    ports = [
      "5432",
      "25432",
      "35432",
    ]
  }
}

resource "google_compute_firewall" "allow-postgres-gitlab-analysis-gitlab-analysis-vpc" {
  name        = "allow-postgres-gitlab-analysis-gitlab-analysis-vpc"
  description = "allow gitlab-analysis network gitlab-analysis-vpc to access gprd network"
  network     = var.network_env

  source_ranges = [
    "10.160.0.0/14", # only from us-west1 default subnet
  ]

  target_tags = [
    "postgres-dr-archive",
    "patroni-v12-zfs",
  ]

  allow {
    protocol = "tcp"
    ports = [
      "5432",
      "25432",
      "35432",
    ]
  }
}

