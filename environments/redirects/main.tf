## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/redirects/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region = "us-east-1"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

locals {
  # Create a flattened redirects list for fastly
  redirects_keys   = flatten([for zone, redirects in var.redirects : [for k, v in redirects : k]])
  redirects_values = flatten([for zone, redirects in var.redirects : [for k, v in redirects : v]])
  redirects        = zipmap(local.redirects_keys, local.redirects_values)
}

module "gitlab-org-redirects-dns-records" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.org."

  cname = {
    for k, v in var.redirects["gitlab.org."] :
    k => {
      ttl     = "300"
      records = [lookup(var.tls_domains, k, "nonssl.global.fastly.net")]
    }
  }
  a = {
    "gitlab.org." = {
      ttl     = "300"
      records = var.tls_apex_domains_ips["gitlab.org"]
    }
  }
}

module "gitlab-io-redirects-dns-records" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.io."

  cname = {
    for k, v in var.redirects["gitlab.io."] :
    k => {
      ttl     = "300"
      records = [lookup(var.tls_domains, k, "nonssl.global.fastly.net")]
    }
  }
  a = {
    "gitlab.io." = {
      ttl     = "300"
      records = var.tls_apex_domains_ips["gitlab.io"]
    }
  }
}

module "gitlab-com-redirects-dns-records" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.com."

  cname = {
    for k, v in var.redirects["gitlab.com."] :
    k => {
      ttl     = "300"
      records = [lookup(var.tls_domains, k, "nonssl.global.fastly.net")]
    }
  }
}

module "allremote-info-apex-redirect-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "allremote.info."

  a = {
    "allremote.info." = {
      ttl     = "300"
      records = var.tls_apex_domains_ips["allremote.info"]
    }
  }
}

module "gitlap-com-redirects-dns-records" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlap.com."

  cname = {
    for k, v in var.redirects["gitlap.com."] :
    k => {
      ttl     = "300"
      records = [lookup(var.tls_domains, k, "nonssl.global.fastly.net")]
    }
  }
}

module "remoteonly-org-redirects-dns-records" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "remoteonly.org."

  cname = {
    for k, v in var.redirects["remoteonly.org."] :
    k => {
      ttl     = "300"
      records = [lookup(var.tls_domains, k, "nonssl.global.fastly.net")]
    }
  }
  a = {
    "remoteonly.org." = {
      ttl     = "300"
      records = var.tls_apex_domains_ips["remoteonly.org"]
    }
  }
}

resource "fastly_service_v1" "redirects" {
  name = "Domain redirects"

  // A dummy backend, all responses from this service are synthetic.
  backend {
    name    = "dummy 127.0.0.1"
    address = "127.0.0.1"
    port    = 80
  }

  dynamic "domain" {
    for_each = merge(local.redirects, var.apex_redirects)

    content {
      name = domain.key
    }
  }

  dynamic "condition" {
    for_each = merge(local.redirects, var.apex_redirects)

    content {
      name      = "${condition.key} request"
      statement = "req.http.Host == \"${condition.key}\""
      type      = "REQUEST"
      priority  = 100
    }
  }

  dynamic "condition" {
    for_each = merge(local.redirects, var.apex_redirects)

    content {
      name      = "${condition.key} response"
      statement = "req.http.Host == \"${condition.key}\""
      type      = "RESPONSE"
      priority  = 100
    }
  }

  dynamic "response_object" {
    for_each = merge(local.redirects, var.apex_redirects)

    content {
      name              = "${response_object.key} response"
      status            = 301
      response          = "Moved Permanently"
      request_condition = "${response_object.key} request"
    }
  }

  dynamic "header" {
    for_each = merge(local.redirects, var.apex_redirects)

    content {
      name               = "${header.key} location"
      action             = "set"
      type               = "response"
      destination        = "http.Location"
      source             = "\"${header.value}\""
      response_condition = "${header.key} response"
    }
  }
}
