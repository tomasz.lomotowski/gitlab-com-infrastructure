variable "project" {
  default = "gitlab-org-ci-0d24e2"
}

variable "region" {
  default = "us-east1"
}

variable "network" {
  default = "org-ci"
}

variable "bootstrap_script_version" {
  default = 9
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-org-ci-chef-bootstrap"
    bootstrap_key     = "gitlab-org-ci-bootstrap-validation"
    bootstrap_keyring = "gitlab-org-ci-bootstrap"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    version           = "14.13.11"
  }
}

variable "chefpem" {
  type        = string
  description = "Path to the chef validator"
}

variable "sleep_time" {
  default     = "120"
  description = "sleep time in seconds to wait before beginning provisioning"
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "environment" {
  default = "org-ci"
}

variable "enable_oslogin" {
  default = true
}

variable "machine_types" {
  type = map(string)

  default = {
    "sd-exporter" = "n1-standard-1"
    "nessus"      = "n1-standard-4"
    "managers"    = "custom-4-16384"
    "bastion"     = "n1-standard-1"
    "gitlab-gke"  = "n1-standard-4"
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "sd-exporter" = 1
    "nessus"      = 1
    "managers"    = 4
    "bastion"     = 1
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "sd-exporter" = []
    "nessus"      = [8834]
    "bastion"     = [22]
  }
}

variable "subnetworks" {
  type = map(string)

  default = {
    "bastion"                 = "10.1.2.0/24"
    "gitlab-gke"              = "10.1.3.0/24"
    "gitlab-gke-pod-cidr"     = "10.3.0.0/16"
    "gitlab-gke-service-cidr" = "10.1.8.0/23"
  }
}

variable "service_account_email" {
  type    = string
  default = "runner-manager@gitlab-org-ci-0d24e2.iam.gserviceaccount.com"
}

variable "runner_manager_role_permissions" {
  type        = list
  description = "a list of permissions that the runner managers compute service account should have"
  default = [
    "compute.disks.create",
    "compute.images.useReadOnly",
    "compute.instances.create",
    "compute.instances.stop",
    "compute.instances.delete",
    "compute.instances.get",
    "compute.instances.getSerialPortOutput",
    "compute.instances.setLabels",
    "compute.instances.setMetadata",
    "compute.instances.setTags",
    "compute.instances.setServiceAccount",
    "compute.subnetworks.use",
    "compute.subnetworks.useExternalIp",
    "compute.networks.updatePolicy",
    "compute.firewalls.create",
    "compute.firewalls.get",
    "compute.zoneOperations.get",
    "compute.projects.get",
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list",
    "cloudkms.cryptoKeyVersions.useToDecrypt"
  ]
}

variable "gcs_service_account_email" {
  type    = string
  default = "storage@gitlab-org-ci-0d24e2.iam.gserviceaccount.com"
}

variable "stale_ci_cleaner_permissions" {
  type        = list
  description = "A list of permissions that the stale runner script uses"
  default = [
    "compute.instances.delete",
    "compute.instances.list",
    "compute.instances.get",
  ]
}

variable "manager_network_tags" {
  default = ["org-ci-manager"]
}

variable "use_new_node_name" {
  default = true
}

variable "bastion_role_permissions" {
  type        = list
  description = "a list of permissions that the runner managers compute service account should have"
  default = [
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list",
    "cloudkms.cryptoKeyVersions.useToDecrypt"
  ]
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.org-ci.gitlab.com"]
}

variable "gitlab_com_zone_id" {
}
# Cloudflare

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}

variable "master_cidr_subnets" {
  type = map(string)

  default = {
    "gitlab-gke" = "172.16.0.0/28"
  }
}

variable "manager_labels" {
  default = {
    gl_resource_type     = "ci_manager"
    runner_manager_group = "gitlab-docker-shared-runners-manager"
  }
}

variable "network_org-ci" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-org-ci-0d24e2/global/networks/org-ci"
}

variable "network_ops" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops"
}
