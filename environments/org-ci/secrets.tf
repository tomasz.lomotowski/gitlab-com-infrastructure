locals {

  secrets = [
    "bastion_org-ci",
    "cookbook-gitlab-runner_org-ci",
    "frontend-loadbalancer_org-ci",
    "gitlab-omnibus-secrets_org-ci",
    "syslog-client_org-ci",
  ]

  common_secret_readers = [
    "serviceAccount:terraform@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
