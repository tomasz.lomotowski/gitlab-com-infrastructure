variable "gitlab_net_zone_id" {}

variable "environment" {
  default = "db-sharding-poc"
}
variable "project" {
  default = "gitlab-db-sharding-poc"
}

variable "region" {
  default = "us-east1"
}

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-db-sharding-poc-chef-bootstrap"
    bootstrap_key     = "gitlab-db-sharding-poc-bootstrap-validation"
    bootstrap_keyring = "gitlab-db-sharding-poc-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "service_account_email" {
  type    = string
  default = "terraform@gitlab-db-sharding-poc.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-db-sharding-poc.iam.gserviceaccount.com"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}
