// Configure remote state
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/aws-snowplow/terraform.tfstate"
    region = "us-east-1"
  }
}

// Use credentials from environment or shared credentials file
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.25"
}

data "archive_file" "snowplow_lambda_event_formatter_archive" {
  type        = "zip"
  source_file = "${path.module}/lambda/lambda_function.py"
  output_path = "${path.module}/lambda/lambda_function_payload.zip"
}

// Policies
resource "aws_iam_policy" "snowplow_collector_policy" {
  description = "Policy the allows the collector to access other AWS services such as Kinesis."
  name        = "snowplow-collector-policy"
  path        = "/"

  policy = file("${path.module}/templates/iam_policy_collector.json")
}

resource "aws_iam_policy" "snowplow_enricher_policy" {
  description = ""
  name        = "snowplow-enricher-policy"
  path        = "/"

  policy = templatefile("${path.module}/templates/iam_policy_enricher.json", {
    geodb_bucket_arn = aws_s3_bucket.snowplow_geodb_bucket.arn
  })
}

resource "aws_iam_policy" "snowplow_lambda_policy" {
  description = ""
  name        = "AWSLambdaBasicExecutionRole-b1df0a33-ac33-47d3-930b-e8e0bf9443ef"
  path        = "/service-role/"

  policy = file("${path.module}/templates/iam_policy_lambda.json")
}

resource "aws_iam_policy" "snowflake_s3_integration_policy" {
  description = ""
  name        = "snowflake-s3-integration"
  path        = "/"

  policy = file("${path.module}/templates/iam_policy_snowflake_s3_integration.json")
}

resource "aws_iam_policy" "workato_s3_integration_policy" {
  description = ""
  name        = "workato-s3-integration"
  path        = "/"

  policy = file("${path.module}/templates/iam_policy_workato_s3_integration.json")
}

resource "aws_iam_role_policy" "snowplow_firehose_enriched_bad_policy" {
  name   = "firehose_enriched_bad"
  policy = file("${path.module}/templates/iam_policy_firehose_enriched_bad.json")
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

resource "aws_iam_role_policy" "snowplow_firehose_raw_bad_policy" {
  name   = "firehose_raw_bad"
  policy = file("${path.module}/templates/iam_policy_firehose_raw_bad.json")
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

resource "aws_iam_role_policy" "snowplow_firehose_enriched_good_policy" {
  name   = "firehose_enriched_good"
  policy = file("${path.module}/templates/iam_policy_firehose_enriched_good.json")
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

// Roles
resource "aws_iam_role" "snowplow_collector_role" {
  name               = "snowplow-collector-role"
  assume_role_policy = file("${path.module}/templates/iam_role_collector.json")

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_iam_role" "snowplow_enricher_role" {
  name               = "snowplow-enricher-role"
  assume_role_policy = file("${path.module}/templates/iam_role_enricher.json")

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_iam_role" "snowplow_lambda_role" {
  name               = "SnowPlowFirehoseFormatter-role-rqkkh478"
  assume_role_policy = file("${path.module}/templates/iam_role_lambda.json")
  path               = "/service-role/"

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_iam_role" "snowplow_firehose_delivery_role" {
  name               = "firehose_delivery_role"
  assume_role_policy = file("${path.module}/templates/iam_role_firehose_delivery.json")
  path               = "/"

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_iam_role" "snowflake_s3_integration_role" {
  name = "snowflake_s3_integration_role"
  assume_role_policy = templatefile(
    "${path.module}/templates/iam_role_snowflake_s3_integration.json", {
      aws_id      = var.snowflake_aws_id,
      external_id = var.snowflake_external_id
    }
  )
  path = "/"

  tags = {
    environment = "SnowPlow"
  }
}

## Workato STAGING role
resource "aws_iam_role" "workato_s3_integration_role" {
  name = "workato_s3_integration_role"
  assume_role_policy = templatefile(
    "${path.module}/templates/iam_role_workato_s3_integration.json", {
      aws_id      = var.workato_aws_id,
      external_id = var.workato_external_stag_id
    }
  )
  path = "/"

  tags = {
    environment = "SnowPlow"
  }
}

## Workato PRODUCTION role
resource "aws_iam_role" "workato_s3_integration_prod_role" {
  name = "workato_s3_integration_prod_role"
  assume_role_policy = templatefile(
    "${path.module}/templates/iam_role_workato_s3_integration.json", {
      aws_id      = var.workato_aws_id,
      external_id = var.workato_external_prod_id
    }
  )
  path = "/"

  tags = {
    environment = "SnowPlow"
  }
}

// Role Policy Attachments
resource "aws_iam_role_policy_attachment" "collector_role_policy_attachment" {
  role       = aws_iam_role.snowplow_collector_role.name
  policy_arn = aws_iam_policy.snowplow_collector_policy.arn
}

resource "aws_iam_role_policy_attachment" "enricher_role_policy_attachment" {
  role       = aws_iam_role.snowplow_enricher_role.name
  policy_arn = aws_iam_policy.snowplow_enricher_policy.arn
}

resource "aws_iam_role_policy_attachment" "lambda_role_policy_attachment" {
  role       = aws_iam_role.snowplow_lambda_role.name
  policy_arn = aws_iam_policy.snowplow_lambda_policy.arn
}

resource "aws_iam_role_policy_attachment" "snowflake_s3_integration_role_policy_attachment" {
  role       = aws_iam_role.snowflake_s3_integration_role.name
  policy_arn = aws_iam_policy.snowflake_s3_integration_policy.arn
}

## Workato STAGING role/policy binding
resource "aws_iam_role_policy_attachment" "workato_s3_integration_role_policy_attachment" {
  role       = aws_iam_role.workato_s3_integration_role.name
  policy_arn = aws_iam_policy.workato_s3_integration_policy.arn
}

## Workato PRODUCTION role/policy binding
resource "aws_iam_role_policy_attachment" "workato_s3_integration_prod_role_policy_attachment" {
  role       = aws_iam_role.workato_s3_integration_prod_role.name
  policy_arn = aws_iam_policy.workato_s3_integration_policy.arn
}

// S3 Buckets

resource "aws_s3_bucket" "snowplow_geodb_bucket" {
  bucket = "gitlab-com-snowplow-geodb"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_s3_bucket" "snowplow_s3_bucket" {
  bucket = "gitlab-com-snowplow-events"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_s3_bucket" "snowplow-datapump_s3_bucket" {
  bucket = "gitlab-com-snowflake-data-pump"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_s3_bucket_policy" "snowplow_s3_bucket_policy" {
  bucket = aws_s3_bucket.snowplow_s3_bucket.id

  policy = file("${path.module}/templates/gitlab-com-snowplow-events.policy.json")
}

resource "aws_s3_bucket_notification" "snowplow_bucket_notifications" {
  bucket = "gitlab-com-snowplow-events"

  queue {
    queue_arn     = "arn:aws:sqs:us-east-1:730570900080:sf-snowpipe-AIDAJ5LKTH5PBBNSL6UC2-14u9uWhgmVmEsNRfTA5B9w"
    events        = ["s3:ObjectCreated:*"]
    filter_prefix = ""
  }
}


// VPC
resource "aws_vpc" "snowplow_vpc" {
  cidr_block = "10.32.0.0/16"

  tags = {
    Name        = "SnowPlow VPC"
    environment = "SnowPlow"
  }
}

// Subnet
resource "aws_subnet" "snowplow_subnet_1" {
  vpc_id            = aws_vpc.snowplow_vpc.id
  cidr_block        = "10.32.2.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name        = "SnowPlow Subnet 1"
    environment = "SnowPlow"
  }
}

resource "aws_subnet" "snowplow_subnet_2" {
  vpc_id            = aws_vpc.snowplow_vpc.id
  cidr_block        = "10.32.1.0/24"
  availability_zone = "us-east-1e"

  tags = {
    Name        = "SnowPlow Subnet 2"
    environment = "SnowPlow"
  }
}

resource "aws_subnet" "snowplow_subnet_3" {
  vpc_id            = aws_vpc.snowplow_vpc.id
  cidr_block        = "10.32.0.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name        = "SnowPlow Subnet 3"
    environment = "SnowPlow"
  }
}

resource "aws_subnet" "snowplow_subnet_4" {
  vpc_id            = aws_vpc.snowplow_vpc.id
  cidr_block        = "10.32.3.0/24"
  availability_zone = "us-east-1c"

  tags = {
    Name        = "SnowPlow Subnet 4"
    environment = "SnowPlow"
  }
}

resource "aws_subnet" "snowplow_subnet_5" {
  vpc_id            = aws_vpc.snowplow_vpc.id
  cidr_block        = "10.32.5.0/24"
  availability_zone = "us-east-1c"

  tags = {
    Name        = "SnowPlow Subnet 5"
    environment = "SnowPlow"
  }
}

// Internet Gateway
resource "aws_internet_gateway" "snowplow_gw" {
  vpc_id = aws_vpc.snowplow_vpc.id

  tags = {
    Name        = "SnowPlow Gateway"
    environment = "SnowPlow"
  }
}

// Routing Tables
resource "aws_route_table" "snowplow_route_table" {
  vpc_id = aws_vpc.snowplow_vpc.id

  tags = {
    Name        = "SnowPlow Routing Table"
    environment = "SnowPlow"
  }
}

resource "aws_main_route_table_association" "snowplow_main_route_table_association" {
  vpc_id         = aws_vpc.snowplow_vpc.id
  route_table_id = aws_route_table.snowplow_route_table.id
}

resource "aws_route" "snowplow_route" {
  route_table_id         = aws_route_table.snowplow_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.snowplow_gw.id
}

resource "aws_route_table_association" "snowplow_route_table_association_1" {
  route_table_id = aws_route_table.snowplow_route_table.id
  subnet_id      = aws_subnet.snowplow_subnet_1.id
}

resource "aws_route_table_association" "snowplow_route_table_association_2" {
  route_table_id = aws_route_table.snowplow_route_table.id
  subnet_id      = aws_subnet.snowplow_subnet_2.id
}

resource "aws_route_table_association" "snowplow_route_table_association_3" {
  route_table_id = aws_route_table.snowplow_route_table.id
  subnet_id      = aws_subnet.snowplow_subnet_3.id
}

resource "aws_route_table_association" "snowplow_route_table_association_4" {
  route_table_id = aws_route_table.snowplow_route_table.id
  subnet_id      = aws_subnet.snowplow_subnet_4.id
}

resource "aws_route_table_association" "snowplow_route_table_association_5" {
  route_table_id = aws_route_table.snowplow_route_table.id
  subnet_id      = aws_subnet.snowplow_subnet_5.id
}

// Security Groups
resource "aws_security_group" "snowplow_security_group" {
  description = "For snowplow stuff"
  name        = "SnowPlow"
  vpc_id      = aws_vpc.snowplow_vpc.id

  egress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]

    ipv6_cidr_blocks = [
      "::/0",
    ]
  }

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    description = "Collector"

    cidr_blocks = [
      "0.0.0.0/0",
    ]

    ipv6_cidr_blocks = [
      "::/0",
    ]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    description = "Enricher"

    cidr_blocks = [
      "0.0.0.0/0",
    ]

    ipv6_cidr_blocks = [
      "::/0",
    ]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH for Admin IPv4"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH for Admin IPv6"

    ipv6_cidr_blocks = [
      "::/0",
    ]
  }

  tags = {
    environment = "SnowPlow"
    Name        = "SnowPlow"
  }
}

resource "aws_security_group" "nessus_scan_target_group" {
  name        = "Scan target security group"
  description = "Allowing inbound scanning from nessus-scanner"
  vpc_id      = aws_vpc.snowplow_vpc.id
  tags = {
    role        = "security-scanner-access"
    environment = "SnowPlow"
  }
}

resource "aws_security_group_rule" "allow_nessus_access" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.nessus_scan_target_group.id
  source_security_group_id = module.nessus_scanner.security_group_id
}

// Kinesis Streams
resource "aws_kinesis_stream" "snowplow_raw_good" {
  name             = "snowplow-raw-good"
  shard_count      = 8
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_stream" "snowplow_raw_bad" {
  name             = "snowplow-raw-bad"
  shard_count      = 8
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_stream" "snowplow_enriched_bad" {
  name             = "snowplow-enriched-bad"
  shard_count      = 8
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_stream" "snowplow_enriched_good" {
  name             = "snowplow-enriched-good"
  shard_count      = 8
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

// EC2 Launch Configs
data "aws_ami" "amazonlinux2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"] # Amazon Images
}

resource "aws_launch_configuration" "snowplow_collector_launch_config" {
  image_id                    = data.aws_ami.amazonlinux2.id
  instance_type               = "t3.micro"
  associate_public_ip_address = "true"
  enable_monitoring           = "false"
  iam_instance_profile        = aws_iam_role.snowplow_collector_role.id
  key_name                    = "snowplow"

  security_groups = [
    aws_security_group.snowplow_security_group.id,
    aws_security_group.nessus_scan_target_group.id
  ]

  user_data = templatefile("${path.module}/templates/collector-user-data.sh", {
    nessus-user = var.nessus-user,
    nessus-key  = var.nessus-key
  })

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "snowplow_enricher_launch_config" {
  image_id                    = data.aws_ami.amazonlinux2.id
  instance_type               = "t2.micro"
  associate_public_ip_address = "true"
  enable_monitoring           = "false"
  iam_instance_profile        = aws_iam_role.snowplow_enricher_role.id
  key_name                    = "snowplow"

  security_groups = [
    aws_security_group.snowplow_security_group.id,
    aws_security_group.nessus_scan_target_group.id
  ]

  user_data = templatefile("${path.module}/templates/enricher-user-data.sh", {
    nessus-user       = var.nessus-user,
    nessus-key        = var.nessus-key,
    geodb_bucket_name = aws_s3_bucket.snowplow_geodb_bucket.id
  })

  lifecycle {
    create_before_destroy = true
  }
}

// EC2 Target Group
resource "aws_lb_target_group" "snowplow_collector_lb_target_group" {
  name     = "SnowPlowNLBTargetGroup"
  port     = 8000
  protocol = "TCP"
  vpc_id   = aws_vpc.snowplow_vpc.id

  health_check {
    interval            = "30"
    path                = "/health"
    protocol            = "HTTP"
    timeout             = "6"
    healthy_threshold   = "3"
    unhealthy_threshold = "3"
  }

  tags = {
    environment = "SnowPlow"
  }
}

// EC2 Auto Scaling Groups
resource "aws_autoscaling_group" "snowplow_collector_autoscaling_group" {
  launch_configuration = aws_launch_configuration.snowplow_collector_launch_config.id
  max_size             = "112"
  min_size             = "3"
  desired_capacity     = "12"

  target_group_arns = [
    aws_lb_target_group.snowplow_collector_lb_target_group.id,
  ]

  vpc_zone_identifier = [
    aws_subnet.snowplow_subnet_1.id,
    aws_subnet.snowplow_subnet_2.id,
    aws_subnet.snowplow_subnet_4.id,
  ]

  enabled_metrics = [
    "GroupStandbyInstances",
    "GroupTotalInstances",
    "GroupPendingInstances",
    "GroupTerminatingInstances",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMinSize",
    "GroupMaxSize",
  ]

  tag {
    key                 = "environment"
    value               = "SnowPlow"
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "SnowPlowAutoCollector"
    propagate_at_launch = true
  }

  lifecycle {
    ignore_changes = [desired_capacity]
  }
}

resource "aws_autoscaling_group" "snowplow_enricher_autoscaling_group" {
  launch_configuration = aws_launch_configuration.snowplow_enricher_launch_config.id
  max_size             = "56"
  min_size             = "3"
  desired_capacity     = "12"

  vpc_zone_identifier = [
    aws_subnet.snowplow_subnet_1.id,
    aws_subnet.snowplow_subnet_2.id,
    aws_subnet.snowplow_subnet_4.id,
  ]

  enabled_metrics = [
    "GroupStandbyInstances",
    "GroupTotalInstances",
    "GroupPendingInstances",
    "GroupTerminatingInstances",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMinSize",
    "GroupMaxSize",
  ]

  tag {
    key                 = "environment"
    value               = "SnowPlow"
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "SnowPlowAutoEnricher"
    propagate_at_launch = true
  }

  lifecycle {
    ignore_changes = [desired_capacity]
  }
}

// Auto Scaling Group Policies
resource "aws_autoscaling_policy" "snowplow_collector_autoscaling_policy" {
  name                   = "snowplow-collector-autoscaling-policy"
  policy_type            = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.snowplow_collector_autoscaling_group.name
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageNetworkIn"
    }
    target_value = 2500000
  }
}

resource "aws_autoscaling_policy" "snowplow_enricher_autoscaling_policy" {
  name                   = "snowplow-enricher-autoscaling-policy"
  policy_type            = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.snowplow_enricher_autoscaling_group.name
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageNetworkIn"
    }
    target_value = 5000000
  }
}

// EC2 Load Balancer
resource "aws_lb" "snowplow_lb" {
  name               = "SnowPlowNLB"
  internal           = false
  load_balancer_type = "network"

  subnets = [
    aws_subnet.snowplow_subnet_1.id,
    aws_subnet.snowplow_subnet_2.id,
    aws_subnet.snowplow_subnet_3.id,
    aws_subnet.snowplow_subnet_4.id,
  ]

  tags = {
    environment = "SnowPlow"
  }
}

// EC2 Load Balancer Listener
resource "aws_lb_listener" "snowplow_collector_lb_listener" {
  load_balancer_arn = aws_lb.snowplow_lb.arn
  port              = "443"
  protocol          = "TLS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-1:855262394183:certificate/d2791575-d352-4e03-895b-c0d554cf6b7a"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.snowplow_collector_lb_target_group.arn
  }
}

// Lambda Function
resource "aws_lambda_function" "snowplow_event_formatter_lambda_function" {
  description   = "This adds a newline to the end of each record."
  function_name = "SnowPlowFirehoseFormatter"
  handler       = "lambda_function.lambda_handler"
  role          = aws_iam_role.snowplow_lambda_role.arn
  runtime       = "python2.7"

  filename         = data.archive_file.snowplow_lambda_event_formatter_archive.output_path
  source_code_hash = data.archive_file.snowplow_lambda_event_formatter_archive.output_base64sha256
  timeout          = "60"

  tags = {
    environment                = "SnowPlow"
    "lambda-console:blueprint" = "kinesis-firehose-process-record-python"
  }
}

// Firehose
resource "aws_kinesis_firehose_delivery_stream" "snowplow_enriched_bad_firehose" {
  destination = "extended_s3"
  name        = "SnowPlowEnrichedBad"

  kinesis_source_configuration {
    kinesis_stream_arn = aws_kinesis_stream.snowplow_enriched_bad.arn
    role_arn           = aws_iam_role.snowplow_firehose_delivery_role.arn
  }

  extended_s3_configuration {
    bucket_arn          = aws_s3_bucket.snowplow_s3_bucket.arn
    role_arn            = aws_iam_role.snowplow_firehose_delivery_role.arn
    compression_format  = "GZIP"
    prefix              = "enriched-bad/"
    error_output_prefix = "enriched-bad/"
    s3_backup_mode      = "Disabled"

    processing_configuration {
      enabled = "true"

      processors {
        type = "Lambda"

        parameters {
          parameter_name  = "LambdaArn"
          parameter_value = "${aws_lambda_function.snowplow_event_formatter_lambda_function.arn}:$LATEST"
        }
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_firehose_delivery_stream" "snowplow_enriched_good_firehose" {
  destination = "extended_s3"
  name        = "SnowPlowEnrichedGood"

  kinesis_source_configuration {
    kinesis_stream_arn = aws_kinesis_stream.snowplow_enriched_good.arn
    role_arn           = aws_iam_role.snowplow_firehose_delivery_role.arn
  }

  extended_s3_configuration {
    bucket_arn          = aws_s3_bucket.snowplow_s3_bucket.arn
    role_arn            = aws_iam_role.snowplow_firehose_delivery_role.arn
    compression_format  = "GZIP"
    prefix              = "output/"
    error_output_prefix = "output/"
    s3_backup_mode      = "Disabled"

    processing_configuration {
      enabled = "true"

      processors {
        type = "Lambda"

        parameters {
          parameter_name  = "LambdaArn"
          parameter_value = "${aws_lambda_function.snowplow_event_formatter_lambda_function.arn}:$LATEST"
        }
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_firehose_delivery_stream" "snowplow_raw_bad_firehose" {
  destination = "extended_s3"
  name        = "SnowPlowRawBad"

  kinesis_source_configuration {
    kinesis_stream_arn = aws_kinesis_stream.snowplow_raw_bad.arn
    role_arn           = aws_iam_role.snowplow_firehose_delivery_role.arn
  }

  extended_s3_configuration {
    bucket_arn          = aws_s3_bucket.snowplow_s3_bucket.arn
    role_arn            = aws_iam_role.snowplow_firehose_delivery_role.arn
    compression_format  = "GZIP"
    prefix              = "raw-bad/"
    error_output_prefix = "raw-bad/"
    s3_backup_mode      = "Disabled"

    processing_configuration {
      enabled = "true"

      processors {
        type = "Lambda"

        parameters {
          parameter_name  = "LambdaArn"
          parameter_value = "${aws_lambda_function.snowplow_event_formatter_lambda_function.arn}:$LATEST"
        }
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

module "nessus_scanner" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/aws-nessus-scanner?ref=v1.0.2"

  scanner_name        = "snowplow-nessus-scanner"
  tenable_linking_key = "064e9fabdbe057bce74bcef638c87a8bffe296b1896251a03b4670342f10fd33"
  vpc_id              = aws_vpc.snowplow_vpc.id
  subnet_id           = aws_subnet.snowplow_subnet_5.id
  instance_type       = "t3.xlarge"
  instance_name       = "snowplow-nessus-scanner"

  instance_tags = {
    role        = "security-scanner"
    projects    = "tenable"
    environment = "SnowPlow"
  }
}
