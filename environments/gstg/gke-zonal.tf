# Warning: for changes to the external DNS module you will need to run:
# `tf apply -target module.gke-external-dns-us-east1-{b,c,d}.google_dns_managed_zone.kube_external_dns` first!
# See https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/merge_requests/1967

## us-east1-b

# In this module contains IP reservations for the GitLab environment
module "gke-reservations-us-east1-b" {
  source                   = "../../modules/gke-reservations"
  name                     = "us-east1-b"
  gke_subnetwork_self_link = module.gke-us-east1-b.subnetwork_self_link
  environment              = var.environment
}

module "gke-us-east1-b" {
  environment = var.environment
  name        = "us-east1-b"
  vpc         = module.network.self_link
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v11.3.0"
  authorized_master_access = [
    "35.237.68.233/32",  # console-01-sv-gstg
    "35.227.116.144/32", # ops.gitlab.net
    "35.231.50.113/32",  # runner-release-01-inf-ops
    "35.185.18.176/32",  # runner-01-inf-ops
  ]

  ip_cidr_range          = var.subnetworks["gitlab-gke-us-east1-b"]
  disable_network_policy = "false"
  dns_zone_name          = var.dns_zone_name
  kubernetes_version     = "1.18"
  maintenance_policy = {
    start_time = "2020-09-14T02:00:00Z"
    end_time   = "2020-09-14T08:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=MO,TU"
  }
  private_master_cidr        = var.master_cidr_subnets["gitlab-gke-us-east1-b"]
  project                    = var.project
  region                     = var.region
  release_channel            = "REGULAR"
  pod_ip_cidr_range          = var.subnetworks["gitlab-gke-pod-cidr-us-east1-b"]
  service_ip_cidr_range      = var.subnetworks["gitlab-gke-service-cidr-us-east1-b"]
  regional_cluster           = "false"
  zone                       = "us-east1-b"
  create_kublet_firewall     = true
  upgrade_notification_queue = google_pubsub_topic.gke_notifications.id
  node_pools = {
    "api-0" = {
      machine_type      = var.machine_types["api"]
      max_node_count    = var.gke_nodepool_max_nodes["api"]
      type              = "api"
      node_auto_upgrade = true
    }
    "default-1" = {
      machine_type      = var.machine_types["gke-default"]
      max_node_count    = var.gke_nodepool_max_nodes["default-zonal"]
      type              = "default"
      node_auto_upgrade = true
    }
    "git-https-1" = {
      machine_type      = var.machine_types["gke-git-https"]
      max_node_count    = var.gke_nodepool_max_nodes["git-https"]
      type              = "git-https"
      node_auto_upgrade = true
    }
    "shell-1" = {
      machine_type      = var.machine_types["gke-shell"]
      max_node_count    = var.gke_nodepool_max_nodes["shell"]
      type              = "shell"
      node_auto_upgrade = true
    }
    "registry-0" = {
      machine_type      = var.machine_types["gke-registry"]
      max_node_count    = var.gke_nodepool_max_nodes["registry"]
      type              = "registry"
      node_auto_upgrade = true
    }
    "websockets-0" = {
      machine_type      = var.machine_types["gke-websockets"]
      max_node_count    = var.gke_nodepool_max_nodes["websockets"]
      type              = "websockets"
      node_auto_upgrade = true
    }
  }
}

module "gke-external-dns-us-east1-b" {
  source      = "../../modules/gke-external-dns"
  name        = "us-east1-b"
  environment = var.environment
  project     = var.project
}

## us-east1-c

# In this module contains IP reservations for the GitLab environment
module "gke-reservations-us-east1-c" {
  source                   = "../../modules/gke-reservations"
  name                     = "us-east1-c"
  gke_subnetwork_self_link = module.gke-us-east1-c.subnetwork_self_link
  environment              = var.environment
}

module "gke-us-east1-c" {
  environment = var.environment
  name        = "us-east1-c"
  vpc         = module.network.self_link
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v11.3.0"
  authorized_master_access = [
    "35.237.68.233/32",  # console-01-sv-gstg
    "35.227.116.144/32", # ops.gitlab.net
    "35.231.50.113/32",  # runner-release-01-inf-ops
    "35.185.18.176/32",  # runner-01-inf-ops
  ]
  ip_cidr_range          = var.subnetworks["gitlab-gke-us-east1-c"]
  disable_network_policy = "false"
  dns_zone_name          = var.dns_zone_name
  kubernetes_version     = "1.18"
  maintenance_policy = {
    start_time = "2020-09-14T02:00:00Z"
    end_time   = "2020-09-14T08:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=TU,WE"
  }
  private_master_cidr        = var.master_cidr_subnets["gitlab-gke-us-east1-c"]
  project                    = var.project
  region                     = var.region
  release_channel            = "REGULAR"
  pod_ip_cidr_range          = var.subnetworks["gitlab-gke-pod-cidr-us-east1-c"]
  service_ip_cidr_range      = var.subnetworks["gitlab-gke-service-cidr-us-east1-c"]
  regional_cluster           = "false"
  zone                       = "us-east1-c"
  create_kublet_firewall     = true
  upgrade_notification_queue = google_pubsub_topic.gke_notifications.id
  node_pools = {
    "api-0" = {
      machine_type      = var.machine_types["api"]
      max_node_count    = var.gke_nodepool_max_nodes["api"]
      type              = "api"
      node_auto_upgrade = true
    }
    "default-1" = {
      machine_type      = var.machine_types["gke-default"]
      max_node_count    = var.gke_nodepool_max_nodes["default-zonal"]
      type              = "default"
      node_auto_upgrade = true
    }
    "git-https-1" = {
      machine_type      = var.machine_types["gke-git-https"]
      max_node_count    = var.gke_nodepool_max_nodes["git-https"]
      type              = "git-https"
      node_auto_upgrade = true
    }
    "shell-1" = {
      machine_type      = var.machine_types["gke-shell"]
      max_node_count    = var.gke_nodepool_max_nodes["shell"]
      type              = "shell"
      node_auto_upgrade = true
    }
    "registry-0" = {
      machine_type      = var.machine_types["gke-registry"]
      max_node_count    = var.gke_nodepool_max_nodes["registry"]
      type              = "registry"
      node_auto_upgrade = true
    }
    "websockets-0" = {
      machine_type      = var.machine_types["gke-websockets"]
      max_node_count    = var.gke_nodepool_max_nodes["websockets"]
      type              = "websockets"
      node_auto_upgrade = true
    }
  }
}

module "gke-external-dns-us-east1-c" {
  source      = "../../modules/gke-external-dns"
  name        = "us-east1-c"
  environment = var.environment
  project     = var.project
}

## us-east1-d

# In this module contains IP reservations for the GitLab environment
module "gke-reservations-us-east1-d" {
  source                   = "../../modules/gke-reservations"
  name                     = "us-east1-d"
  gke_subnetwork_self_link = module.gke-us-east1-d.subnetwork_self_link
  environment              = var.environment
}

module "gke-us-east1-d" {
  environment = var.environment
  name        = "us-east1-d"
  vpc         = module.network.self_link
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v11.3.0"
  authorized_master_access = [
    "35.237.68.233/32",  # console-01-sv-gstg
    "35.227.116.144/32", # ops.gitlab.net
    "35.231.50.113/32",  # runner-release-01-inf-ops
    "35.185.18.176/32",  # runner-01-inf-ops
  ]
  ip_cidr_range          = var.subnetworks["gitlab-gke-us-east1-d"]
  disable_network_policy = "false"
  dns_zone_name          = var.dns_zone_name
  kubernetes_version     = "1.18"
  maintenance_policy = {
    start_time = "2020-09-14T02:00:00Z"
    end_time   = "2020-09-14T08:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=TU,WE"
  }
  private_master_cidr        = var.master_cidr_subnets["gitlab-gke-us-east1-d"]
  project                    = var.project
  region                     = var.region
  release_channel            = "REGULAR"
  pod_ip_cidr_range          = var.subnetworks["gitlab-gke-pod-cidr-us-east1-d"]
  service_ip_cidr_range      = var.subnetworks["gitlab-gke-service-cidr-us-east1-d"]
  regional_cluster           = "false"
  upgrade_notification_queue = google_pubsub_topic.gke_notifications.id
  zone                       = "us-east1-d"
  create_kublet_firewall     = true
  node_pools = {
    "api-0" = {
      machine_type      = var.machine_types["api"]
      max_node_count    = var.gke_nodepool_max_nodes["api"]
      type              = "api"
      node_auto_upgrade = true
    }
    "default-1" = {
      machine_type      = var.machine_types["gke-default"]
      max_node_count    = var.gke_nodepool_max_nodes["default-zonal"]
      type              = "default"
      node_auto_upgrade = true
    }
    "git-https-1" = {
      machine_type      = var.machine_types["gke-git-https"]
      max_node_count    = var.gke_nodepool_max_nodes["git-https"]
      type              = "git-https"
      node_auto_upgrade = true
    }
    "shell-1" = {
      machine_type      = var.machine_types["gke-shell"]
      max_node_count    = var.gke_nodepool_max_nodes["shell"]
      type              = "shell"
      node_auto_upgrade = true
    }
    "registry-0" = {
      machine_type      = var.machine_types["gke-registry"]
      max_node_count    = var.gke_nodepool_max_nodes["registry"]
      type              = "registry"
      node_auto_upgrade = true
    }
    "websockets-0" = {
      machine_type      = var.machine_types["gke-websockets"]
      max_node_count    = var.gke_nodepool_max_nodes["websockets"]
      type              = "websockets"
      node_auto_upgrade = true
    }
  }
}

module "gke-external-dns-us-east1-d" {
  source      = "../../modules/gke-external-dns"
  name        = "us-east1-d"
  environment = var.environment
  project     = var.project
}
