resource "google_compute_address" "fe_external_ip" {
  name         = "http"
  project      = var.project
  region       = var.region
  address_type = "EXTERNAL"

  # subnetwork   = var.subnetwork
}
