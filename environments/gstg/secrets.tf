locals {

  secrets = [
    "bastion_gstg",
    "blackbox-exporter_gstg",
    "camoproxy_gstg",
    "contributors_gstg",
    "cookbook-gitlab-runner_gstg",
    "elasticsearch-exporter_gstg",
    "elk_gstgfrontend-loadbalancer_gstg",
    "gitlab-consul_gstg-client",
    "gitlab-consul_gstg-cluster",
    "gitlab-elk_gstg",
    "gitlab-gstg-secrets_gitlab-walg_gstg",
    "gitlab-migration_gstg",
    "gitlab-monitor_gstg",
    "gitlab-okta-asa_gstg",
    "gitlab-omnibus-secrets-geo_gstg",
    "gitlab-omnibus-secrets_gstg",
    "gitlab-patroni_gstg",
    "gitlab-pgbouncer_gstg",
    "gitlab-prometheus-secrets_gstg",
    "gitlab-wale_gstg",
    "gitlab-walg_gstg",
    "imap-mailbox-exporter_gstg",
    "pgbouncer_gstg",
    "postgres-exporter_gstg",
    "praefect-omnibus-secrets_gstg",
    "prometheus-alertmanager_gstg",
    "redis_analysis_gstg",
    "syslog-client_gstg",
    "version-gitlab-com_gstg",
  ]

  common_secret_readers = [
    "serviceAccount:terraform@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
