module "cloudflare-embargo-country-blocklist" {
  source  = "../../modules/cloudflare-embargo-country-blocklist"
  zone_id = var.cloudflare_zone_id
}
