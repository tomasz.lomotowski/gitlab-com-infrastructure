##################################
#
#  gitlab-analysis
#
#################################

resource "google_compute_network_peering" "peering-gitlab-analysis-default" {
  name         = "peering-gitlab-analysis-default"
  network      = var.network_env
  peer_network = "https://www.googleapis.com/compute/v1/projects/gitlab-analysis/global/networks/default"
}

resource "google_compute_network_peering" "peering-gitlab-analysis-gitlab-analysis-vpc" {
  name         = "peering-gitlab-analysis-gitlab-analysis-vpc"
  network      = var.network_env
  peer_network = "https://www.googleapis.com/compute/v1/projects/gitlab-analysis/global/networks/gitlab-analysis-vpc"
}

resource "google_compute_firewall" "allow-postgres-gitlab-analysis-default" {
  name        = "allow-postgres-gitlab-analysis-default"
  description = "allow gitlab-analysis network default to access gstg network"
  network     = var.network_env

  source_ranges = [
    "10.52.0.0/14", # only from us-west-1 default subnet
  ]

  target_tags = [
    "postgres-dr-archive",
    "patroni-zfs",
  ]

  allow {
    protocol = "tcp"
    ports = [
      "5432",
      "25432",
    ]
  }
}

resource "google_compute_firewall" "allow-postgres-gitlab-analysis-gitlab-analysis-vpc" {
  name        = "allow-postgres-gitlab-analysis-gitlab-analysis-vpc"
  description = "allow gitlab-analysis network gitlab-analysis-vpc to access gstg network"
  network     = var.network_env

  source_ranges = [
    "10.160.0.0/14", # only from us-west-1 default subnet
  ]

  target_tags = [
    "postgres-dr-archive",
    "patroni-zfs",
  ]

  allow {
    protocol = "tcp"
    ports = [
      "5432",
      "25432",
    ]
  }
}

