##################################
#
#  GKE Cluster for gstg GitLab services
#
##################################

# Service account utilized by CI
resource "google_service_account" "k8s-workloads" {
  account_id   = "k8s-workloads"
  display_name = "k8s-workloads"
}

# Service account utilized by CI for Read Only operations
resource "google_service_account" "k8s-workloads-ro" {
  account_id   = "k8s-workloads-ro"
  display_name = "k8s-workloads-ro"
}

resource "google_project_iam_custom_role" "k8s-workloads" {
  project     = var.project
  role_id     = "k8sWorkloads"
  title       = "k8s-workloads"
  permissions = ["clientauthconfig.clients.listWithSecrets", "container.secrets.list"]
}

resource "google_project_iam_binding" "k8s-workloads" {
  project = var.project
  role    = "projects/${var.project}/roles/${google_project_iam_custom_role.k8s-workloads.role_id}"

  members = [
    "serviceAccount:${google_service_account.k8s-workloads.email}",
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}"
  ]
}

# Reserved IP address for prometheus operator
resource "google_compute_address" "prometheus-gke" {
  name         = "prometheus-gke-${var.environment}"
  description  = "prometheus-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for prometheus ingress with IAP
resource "google_compute_global_address" "prometheus-ingress-iap" {
  name        = "prometheus-ingress-iap-${var.environment}"
  description = "prometheus-ingress-iap-${var.environment}"
}

module "prometheus-gke-gstg-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "prometheus-gke.gstg.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.prometheus-ingress-iap.address]
    }
  }
}

# Reserved IP address for thanos-query
resource "google_compute_address" "thanos-query-gke" {
  name         = "thanos-query-gke-${var.environment}"
  description  = "thanos-query-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for plantuml, must be global

resource "google_compute_global_address" "plantuml-gke" {
  name        = "plantuml-gke-${var.environment}"
  description = "plantuml-gke-${var.environment}"
}

module "plantuml-gke-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab-static.net."
  a = {
    "gstg.plantuml.gitlab-static.net." = {
      ttl = "300"
      records = [
        google_compute_global_address.plantuml-gke.address
      ]
    }
  }
}

# Reserved IP address for jaeger

resource "google_compute_global_address" "jaeger-iap" {
  name        = "jaeger-iap-${var.environment}"
  description = "jaeger-iap-${var.environment}"
}

module "jaeger-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "jaeger.gstg.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.jaeger-iap.address]
    }
  }
}

# Reserved IP address for woodhouse

resource "google_compute_global_address" "woodhouse" {
  name        = "woodhouse-${var.environment}"
  description = "IP for Woodhouse's GKE ingress, annotation-bound in tanka."
}

module "woodhouse_dns_record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "woodhouse.${var.environment}.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.woodhouse.address]
    }
  }
}

module "gitlab-gke" {
  environment = var.environment
  name        = "gitlab-gke"
  vpc         = module.network.self_link
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v11.3.0"

  authorized_master_access = [
    "35.237.68.233/32",  # console-01-sv-gstg
    "35.227.116.144/32", # ops.gitlab.net
    "35.231.50.113/32",  # runner-release-01-inf-ops
    "35.185.18.176/32",  # runner-01-inf-ops
  ]

  ip_cidr_range          = var.subnetworks["gitlab-gke"]
  disable_network_policy = "false"
  dns_zone_name          = var.dns_zone_name
  kubernetes_version     = "1.18"
  maintenance_policy = {
    start_time = "2020-09-14T02:00:00Z"
    end_time   = "2020-09-14T08:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=TU,WE"
  }
  nodes_service_account_name = "gitlab-gke-nodes"
  private_master_cidr        = var.master_cidr_subnets["gitlab-gke"]
  project                    = var.project
  region                     = var.region
  release_channel            = "REGULAR"
  pod_ip_cidr_range          = var.subnetworks["gitlab-gke-pod-cidr"]
  service_ip_cidr_range      = var.subnetworks["gitlab-gke-service-cidr"]
  subnet_nat_name            = "gitlab-gke-gstg"
  upgrade_notification_queue = google_pubsub_topic.gke_notifications.id
  workload_identity_ksa = {
    "gitlab/gitlab-webservice" = ["roles/cloudprofiler.agent"],
    "gitlab/gitlab-registry"   = ["roles/cloudprofiler.agent"]
  }

  node_pools = {
    "catchall-0" = {
      machine_type      = var.machine_types["gke-catchall"]
      max_node_count    = var.gke_nodepool_max_nodes["catchall"]
      type              = "catchall"
      node_auto_upgrade = true
    }
    "default-1" = {
      machine_type      = var.machine_types["gke-default"]
      max_node_count    = var.gke_nodepool_max_nodes["default-regional"]
      type              = "default"
      node_auto_upgrade = true
    }
    "sidekiq-memory-bound-1" = {
      machine_type      = var.machine_types["gke-memory-bound"]
      max_node_count    = var.gke_nodepool_max_nodes["memory-bound"]
      type              = "memory-bound"
      node_auto_upgrade = true
    }
    "sidekiq-low-urgency-cpu-bound-1" = {
      machine_type      = var.machine_types["gke-low-urgency-cpu-bound"]
      max_node_count    = var.gke_nodepool_max_nodes["low-urgency-cpu-bound"]
      node_disk_type    = "pd-standard"
      type              = "low-urgency-cpu-bound"
      node_auto_upgrade = true
    }
    "sidekiq-urgent-other-1" = {
      machine_type      = var.machine_types["gke-urgent-other"]
      max_node_count    = var.gke_nodepool_max_nodes["urgent-other"]
      node_disk_type    = "pd-standard"
      type              = "urgent-other"
      node_auto_upgrade = true
    }
    "sidekiq-urgent-cpu-bound-1" = {
      machine_type      = var.machine_types["gke-urgent-cpu-bound"]
      max_node_count    = var.gke_nodepool_max_nodes["urgent-cpu-bound"]
      node_disk_type    = "pd-standard"
      type              = "urgent-cpu-bound"
      node_auto_upgrade = true
    }
    "kas-1" = {
      machine_type      = var.machine_types["gke-kas"]
      max_node_count    = var.gke_nodepool_max_nodes["kas"]
      node_disk_type    = "pd-standard"
      type              = "kas"
      node_auto_upgrade = true
    }
  }
}

module "gke-external-dns" {
  source = "../../modules/gke-external-dns"

  environment = var.environment
  project     = var.project
}

// Allow GKE to talk to the prometheus operator which utilizes port 8443
resource "google_compute_firewall" "gke-master-to-kubelet" {
  name    = "k8s-api-to-kubelets"
  network = module.network.self_link
  project = var.project

  description = "GKE API to kubelets"

  source_ranges = [var.master_cidr_subnets["gitlab-gke"]]

  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }

  target_tags = ["gitlab-gke"]
}

resource "google_compute_firewall" "consul-deny-internal" {
  name    = format("%v-consul-deny-internal", var.environment)
  network = var.environment

  priority  = "500" # must come before any other standard allows (typically priority 1000)
  direction = "INGRESS"

  deny {
    protocol = "tcp"

    ports = [
      "8500",
    ]
  }
}

// Reserved IP address for KAS
resource "google_compute_global_address" "kas-gke" {
  name        = "kas-gke-gstg"
  description = "kas-gke-gstg"
}

// cloudflare setup for kas.staging.gitlab.com
module "kas-staging-gitlab-com-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "staging.gitlab.com."

  a = {
    "kas.staging.gitlab.com." = {
      ttl     = "300"
      records = [google_compute_global_address.kas-gke.address]
      "spectrum_config" = {
        "proxy_protocol" = "off"
        "ports" = {
          "443" = "443",
        }
      }
    }
  }
}

// Reserved IP for KAS internal ingress
resource "google_compute_address" "kas-internal-gke" {
  name         = "kas-internal-gke-gstg"
  description  = "kas-internal-gke-gstg"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

// DNS record for KAS Internal
module "kas-int-gstg-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "kas.int.gstg.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_address.kas-internal-gke.address]
    }
  }
}

// Pubsub topic for gke-notifications
resource "google_pubsub_topic" "gke_notifications" {
  name = "gke_notifications"
}

// Cloud function to get upgrade notifications/annotations from gke
resource "google_cloudfunctions_function" "gke_notifications" {
  entry_point = "HelloPubSub"
  environment_variables = {
    ENVIRONMENT     = var.environment,
    GRAFANA_API_KEY = var.grafana_api_key,
    GRAFANA_URL     = "https://dashboards.gitlab.net/api/annotations",
  }
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.gke_notifications.id
  }
  name                  = "gke_notifications"
  project               = var.project
  region                = var.region
  runtime               = "go113"
  source_archive_bucket = "gitlab-gke-notifications-function"
  source_archive_object = "gke-notifications.zip"
}

data "cloudflare_ip_ranges" "cloudflare" {}

// Cloud Armor policy for kas.staging.gitlab.com
resource "google_compute_security_policy" "kas-ingress-policy" {
  name = "kas-ingress-policy"
  rule {
    action   = "deny(403)"
    priority = "2147483647"
    match {
      versioned_expr = "SRC_IPS_V1"
      config {
        src_ip_ranges = ["*"]
      }
    }
    description = "default rule block all IPs"
  }
  dynamic "rule" {
    for_each = chunklist([for s in sort(data.cloudflare_ip_ranges.cloudflare.ipv4_cidr_blocks) : s], 10)
    content {
      action   = "allow"
      priority = 1000 + rule.key
      match {
        versioned_expr = "SRC_IPS_V1"
        config {
          src_ip_ranges = rule.value
        }
      }
      description = "allow Cloudflare CIDR blocks ${rule.key + 1}"
    }
  }
}
