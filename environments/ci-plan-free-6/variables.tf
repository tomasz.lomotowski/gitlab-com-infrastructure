variable "project" {
  default = "gitlab-ci-plan-free-6-f2de7a"
}

variable "network" {
  default = "ephemeral-runners"
}
