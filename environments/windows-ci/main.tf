terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/windows-ci/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

##################################
#
#  Networking
#
##################################

# Didn't use the VPC module here because it creates firewall rules we don't want
resource "google_compute_network" "windows-ci-network" {
  name                    = var.network
  auto_create_subnetworks = false
}

module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.3.0"

  log_level        = "ALL"
  nat_ip_count     = 10
  nat_ports_per_vm = 256
  region           = var.region
  network_name     = google_compute_network.windows-ci-network.name
}

resource "google_compute_subnetwork" "manager-subnet" {
  name          = "manager-subnet"
  ip_cidr_range = "10.1.0.0/16"
  region        = var.region
  network       = google_compute_network.windows-ci-network.self_link
}

resource "google_compute_subnetwork" "executor-subnet" {
  name                     = "executor-subnet"
  ip_cidr_range            = "10.2.0.0/16"
  region                   = var.region
  private_ip_google_access = true
  network                  = google_compute_network.windows-ci-network.self_link
}

##################################
#
#  Service Accounts
#
##################################

resource "google_service_account" "runner-manager" {
  account_id   = "runner-manager"
  display_name = "Runner Managers Account"
  description  = "Service account used by the runners manager to spin up executor VMs"
}

resource "google_project_iam_custom_role" "runner-manager" {
  role_id     = "runnerManager"
  title       = "Runner Manager Role"
  description = "Compute service account for Windows CI runner managers"
  permissions = var.runner_manager_role_permissions
}

resource "google_project_iam_member" "runner-manager" {
  role   = google_project_iam_custom_role.runner-manager.id
  member = "serviceAccount:${google_service_account.runner-manager.email}"
}

resource "google_project_iam_custom_role" "packer-runner" {
  role_id     = "packerRunner"
  title       = "Packer Runner Role"
  description = "Compute service account for runner used for packer and ansible"
  permissions = var.packer_runner_role_permissions
}

resource "google_service_account" "packer-runner" {
  account_id   = "packer-runner"
  display_name = "Packer Runner Account"
  description  = "Service account used by packer running in a linux runner to create Window images"
}

resource "google_project_iam_member" "packer-runner" {
  role   = google_project_iam_custom_role.packer-runner.id
  member = "serviceAccount:${google_service_account.packer-runner.email}"
}

resource "google_project_iam_member" "iam-service-account-user" {
  role   = "roles/iam.serviceAccountUser"
  member = "serviceAccount:${google_service_account.packer-runner.email}"
}

resource "google_service_account" "runner-cache" {
  account_id   = "runner-cache"
  display_name = "Access to GCS for Windows runners cache"
  description  = "Service account used by the runner managers for distributed cache"
}

resource "google_project_iam_member" "storage-object-admin" {
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.runner-cache.email}"
}

resource "google_service_account" "bastion" {
  account_id   = "bastion"
  display_name = "bastion Account"
  description  = "Service account used by the bastion server. It should have minimal privileges."
}

resource "google_project_iam_custom_role" "bastion" {
  role_id     = "bastion"
  title       = "Bastion Role"
  description = "Compute service account for bastion servers"
  permissions = var.bastion_role_permissions
}

resource "google_project_iam_member" "bastion" {
  role   = google_project_iam_custom_role.bastion.id
  member = "serviceAccount:${google_service_account.bastion.email}"
}

resource "google_service_account" "stale-ci-cleaner" {
  account_id   = "stale-ci-cleaner"
  display_name = "Stale CI Runners Cleaner Account"
  description  = "Service account used by the script that cleans up stale CI runners"
  # https://ops.gitlab.net/gitlab-com/gl-infra/ci-project-cleaner
}

resource "google_project_iam_custom_role" "stale-ci-cleaner" {
  role_id     = "staleCiCleaner"
  title       = "Stale CI Runners Cleaner role"
  description = "Role for the script that cleans up stale CI runners"
  permissions = var.stale_ci_cleaner_permissions
}

resource "google_project_iam_member" "stale-ci-cleaner" {
  role   = google_project_iam_custom_role.stale-ci-cleaner.id
  member = "serviceAccount:${google_service_account.stale-ci-cleaner.email}"
}

# Developers/SRE are added to this group.
resource "google_project_iam_member" "gcp-ci-ops" {
  role   = "roles/owner"
  member = "group:gcp-ci-ops-sg@gitlab.com"
}

###############################################
#
# Load balancer and VM for the ops bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  os_boot_image         = var.os_boot_image["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = google_service_account.bastion.email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.1.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = google_compute_network.windows-ci-network.self_link
}

##################################
#
#  Runner Mangers Controller
#
##################################

module "runner" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-manager-controller"]
  machine_type          = var.machine_types["runner-manager-controller"]
  os_boot_image         = var.os_boot_image["runner-manager-controller"]
  name                  = "runner"
  node_count            = var.node_count["runner-manager-controller"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["runner-manager-controller"]
  region                = var.region
  service_account_email = google_service_account.packer-runner.email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.1.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = google_compute_network.windows-ci-network.self_link
  additional_scopes     = ["compute-rw", "storage-full"]
}

##################################
#
#  Object storage buckets
#
##################################

###
### The storage bucket module creates a bunch of buckets we don't need.
### It is designed for a GitLab installation, not what we're doing here.
###

resource "google_storage_bucket" "secrets" {
  name     = "gitlab-${var.environment}-secrets"
  location = var.storage_location

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-secrets"
  }

  versioning {
    enabled = var.secrets_versioning
  }

  storage_class = var.storage_class
}

resource "google_storage_bucket_iam_binding" "secrets-binding" {
  bucket = "gitlab-${var.environment}-secrets"
  role   = "roles/storage.objectViewer"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_storage_bucket" "chef-bootstrap" {
  name     = "gitlab-${var.environment}-chef-bootstrap"
  location = var.storage_location

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-${var.environment}-chef-bootstrap"
  }

  versioning {
    enabled = var.chef_bootstrap_versioning
  }

  storage_class = var.storage_class
}

resource "google_storage_bucket_iam_binding" "chef-bootstrap-binding" {
  bucket     = "gitlab-${var.environment}-chef-bootstrap"
  role       = "roles/storage.objectViewer"
  depends_on = [google_storage_bucket.chef-bootstrap]

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_storage_bucket" "shared-runners-cache" {
  name     = "gitlab-com-windows-runner-cache"
  location = var.storage_location

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-com-windows-runner-cache"
  }

  storage_class = var.storage_class
}


##################################
#
#  Keyrings
#
##################################

# Create the bootstrap KMS key ring
resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = google_kms_key_ring.bootstrap.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

# Create the gitlab-secrets KMS key ring
resource "google_kms_key_ring" "gitlab-secrets" {
  name     = "gitlab-secrets"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = google_kms_key_ring.gitlab-secrets.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

##################################
#
#  Windows Runner Managers
#
##################################

data "google_compute_image" "runner-manager" {
  name    = var.os_image_name
  project = var.project
}

module "runner-manager" {
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/windows-runner.git?ref=v1.1.0"
  name                  = var.manager_name
  environment           = var.environment
  os_image              = data.google_compute_image.runner-manager.self_link
  network_name          = google_compute_network.windows-ci-network.self_link
  subnetwork_name       = google_compute_subnetwork.manager-subnet.self_link
  additional_scopes     = var.additional_manager_scopes
  service_account_email = var.service_account_email
  use_external_ip       = true
  assign_public_ip      = true
  node_count            = var.node_count["runner-manager"]
  tags                  = ["windows-manager"]
  labels                = var.manager_labels
}
