variable "project" {
  default = "gitlab-ci-windows"
}

variable "region" {
  default = "us-east1"
}

variable "network" {
  default = "windows-ci"
}

variable "bootstrap_script_version" {
  default = 9
}

# See 1password
variable "miner_ips" {
  type    = list(string)
  default = []
}

variable "environment" {
  default = "windows-ci"
}

variable "service_account_email" {
  type    = string
  default = "runner-manager@gitlab-ci-windows.iam.gserviceaccount.com"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-windows-ci-chef-bootstrap"
    bootstrap_key     = "gitlab-windows-ci-validation"
    bootstrap_keyring = "gitlab-windows-ci-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "secrets_versioning" {
  default = false
}

variable "storage_class" {
  default = "REGIONAL"
}

variable "storage_location" {
  default = "US-EAST1"
}

variable "chef_bootstrap_versioning" {
  default = false
}

variable "runner_manager_role_permissions" {
  type        = list(any)
  description = "a list of permissions that the runner managers compute service account should have"
  default = [
    "compute.disks.create",
    "compute.images.useReadOnly",
    "compute.instances.create",
    "compute.instances.delete",
    "compute.instances.get",
    "compute.instances.getSerialPortOutput",
    "compute.instances.setLabels",
    "compute.instances.setMetadata",
    "compute.instances.setTags",
    "compute.subnetworks.use",
    "compute.subnetworks.useExternalIp",
    "compute.zoneOperations.get",
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list",
    "cloudkms.cryptoKeyVersions.useToDecrypt"
  ]
}

variable "packer_runner_role_permissions" {
  type        = list(any)
  description = "a list of permissions that the runner building the images account should have"
  default = [
    "compute.disks.create",
    "compute.disks.delete",
    "compute.disks.useReadOnly",
    "compute.globalOperations.get",
    "compute.images.create",
    "compute.images.delete",
    "compute.images.get",
    "compute.instances.create",
    "compute.instances.delete",
    "compute.instances.get",
    "compute.instances.getSerialPortOutput",
    "compute.instances.list",
    "compute.instances.setMetadata",
    "compute.instances.setServiceAccount",
    "compute.instances.setTags",
    "compute.machineTypes.get",
    "compute.subnetworks.use",
    "compute.subnetworks.useExternalIp",
    "compute.zoneOperations.get",
    "compute.zones.get",
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list",
    "cloudkms.cryptoKeyVersions.useToDecrypt"
  ]
}

variable "bastion_role_permissions" {
  type        = list(any)
  description = "a list of permissions that the runner managers compute service account should have"
  default = [
    "storage.buckets.get",
    "storage.objects.get",
    "storage.objects.list",
    "cloudkms.cryptoKeyVersions.useToDecrypt"
  ]
}

variable "stale_ci_cleaner_permissions" {
  type        = list(any)
  description = "A list of permissions that the stale runner script uses"
  default = [
    "compute.instances.delete",
    "compute.instances.list",
    "compute.instances.get",
  ]
}

variable "manager_name" {
  type        = string
  description = "Name for the managers"
  default     = "windows-shared-runners-manager"
}

variable "manager_labels" {
  default = {
    runner_manager = "windows-shared-runners-manager"
    runner_type    = "manager"
  }
}

variable "os_image_name" {
  type        = string
  description = "Boot image name"
  default     = "windows-manager-testing"
}

variable "os_boot_image" {
  description = "Boot image to use for Linux servers"
  type        = map(string)

  default = {
    "bastion"                   = "ubuntu-os-cloud/ubuntu-2004-lts"
    "runner-manager-controller" = "ubuntu-os-cloud/ubuntu-2004-lts"
  }
}

variable "additional_manager_scopes" {
  type        = list(any)
  description = "Additional scopes for the runner managers"
  default = [
    "compute-rw",
    "storage-ro"
  ]
}

variable "subnetworks" {
  type = map(string)

  default = {
    "bastion"                   = "10.3.1.0/24"
    "runner-manager-controller" = "10.3.0.0/24"
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "bastion"                   = [22]
    "runner-manager-controller" = []
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "bastion"                   = 1
    "runner-manager"            = 2
    "runner-manager-controller" = 1
  }
}

variable "machine_types" {
  type = map(string)

  default = {
    "bastion"                   = "n1-standard-1"
    "runner-manager-controller" = "n1-standard-2"
  }
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.windows-ci.gitlab.com"]
}

variable "gitlab_com_zone_id" {
}
# Cloudflare

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}
