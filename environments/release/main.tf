## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/release/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## CloudFlare
provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

## Local
provider "local" {
  version = "~> 1.4.0"
}

## Google
provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

##################################
#
#  Network
#
#################################

module "network" {
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  project          = var.project
  environment      = var.environment
  internal_subnets = var.internal_subnets
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = length(var.peer_networks["names"])
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = var.network_env
  peer_network = element(var.peer_networks["links"], count.index)
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.13.0"
  project                           = var.project
  prometheus_workload_identity      = false
}

##################################
#
# all-in-one instance
#
#################################

module "release-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "release.gitlab.net." = {
      ttl     = "300"
      records = module.all-in-one.instances.*.network_interface.0.access_config.0.nat_ip
      spectrum_config = {
        proxy_protocol = "off",
        ip_firewall    = true,
        ports = {
          "22"  = "22",
          "80"  = "http",
          "443" = "https"
        }
      }
    },
    "registry.release.gitlab.net." = {
      ttl     = "300"
      records = module.all-in-one.instances.*.network_interface.0.access_config.0.nat_ip
    }
  }
}

module "all-in-one" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-gitlab]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["all-in-one"]
  machine_type          = var.machine_types["all-in-one"]
  name                  = "release"
  node_count            = var.node_count["all-in-one"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["all-in-one"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}
