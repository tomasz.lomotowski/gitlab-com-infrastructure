variable "project" {
  default = "gitlab-ci-plan-free-3-35411a"
}

variable "network" {
  default = "ephemeral-runners"
}
