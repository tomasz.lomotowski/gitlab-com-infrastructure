#!/usr/bin/env bash
set -e

DIR="$(mktemp -d)"
cd "$(dirname "${BASH_SOURCE[0]}")/.."

cleanup () {
  rm -rf "${DIR}"
  cd - 2>&1 >/dev/null
}

trap cleanup EXIT SIGINT

echo 'cleaning up workspace...'
rm -rf .terraform
tf init

echo 'planning... (this might take a while, kick back and relax!)'
tf plan $@ -no-color -out "${DIR}/mothership"

echo "generating json..."
tf show -json "${DIR}/mothership" > "${DIR}/mothership.json"

echo "calculating moves..."
ruby "scripts/replacer.rb" "${DIR}/mothership.json" > generated_move_state.sh

echo "Did the script bark at you? No? Good. Yes? Try fixing it (or ignore^^)"
echo "Review the generated_move_state.sh. When confident, execute to move the state"
