// Configure remote state
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/aws-account/terraform.tfstate"
    region = "us-east-1"
  }
}

// Use credentials from environment or shared credentials file
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27"
}

module "cloudtrail" {
  source                        = "git::https://github.com/cloudposse/terraform-aws-cloudtrail.git?ref=0.9.0"
  namespace                     = "gitlab"
  stage                         = "aws"
  name                          = "cloudtrail-default"
  enable_logging                = "true"
  enable_log_file_validation    = "true"
  include_global_service_events = "true"
  is_multi_region_trail         = "true"
  s3_bucket_name                = module.cloudtrail_s3_bucket.bucket_id
}

module "cloudtrail_s3_bucket" {
  source    = "git::https://github.com/cloudposse/terraform-aws-cloudtrail-s3-bucket.git?ref=0.1.1"
  namespace = "gitlab"
  stage     = "aws"
  name      = "cloudtrail-default"
  region    = "us-east-1"
}

resource "aws_s3_bucket" "forum_backup" {
  bucket = "gitlab-discourse-forum-backup"
  acl    = "private"
}

resource "aws_iam_user" "forum_backup" {
  name = "forum-backup"
}

resource "aws_iam_access_key" "forum_backup" {
  user = aws_iam_user.forum_backup.name
}

resource "aws_iam_policy" "write_forum_backup" {
  name = "write-forum-backup"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::${aws_s3_bucket.forum_backup.bucket}",
        "arn:aws:s3:::${aws_s3_bucket.forum_backup.bucket}/*"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_user_policy_attachment" "forum_backup" {
  user       = aws_iam_user.forum_backup.name
  policy_arn = aws_iam_policy.write_forum_backup.arn
}

/*
IAM account for cross-account access to provision EKS clusters in user's AWS
environment(s) from gitlab.com
*/

resource "aws_iam_user" "eks-provisioner" {
  name = "eks-provisioner"
}

resource "aws_iam_access_key" "eks-provisioner" {
  user = aws_iam_user.eks-provisioner.name
}

resource "aws_iam_policy" "sts-assumerole-eks-provisioner" {
  name = "sts-assumerole-eks-provisioner"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "NotResource": [
      "arn:aws:iam::855262394183:role/*"
    ],
    "Condition":{"Null":{"sts:ExternalId":"false"}}
  }
}
EOF

}

resource "aws_iam_user_policy_attachment" "eks-provisioner" {
  user       = aws_iam_user.eks-provisioner.name
  policy_arn = aws_iam_policy.sts-assumerole-eks-provisioner.arn
}

#######################################################
#
# Tenable.IO local Nessus scanner
#
#######################################################

data "aws_ami" "ubuntu-xenial" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-20200129"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "nessus-scanner" {
  ami                         = data.aws_ami.ubuntu-xenial.id
  associate_public_ip_address = true
  disable_api_termination     = true
  get_password_data           = false
  instance_type               = "t2.xlarge"
  monitoring                  = false
  source_dest_check           = true
  key_name                    = "cpallares"
  subnet_id                   = aws_subnet.nessus.id
  depends_on                  = ["aws_internet_gateway.nessus"]
  tags                        = var.nessus_tags
  vpc_security_group_ids = [
    aws_security_group.nessus.id
  ]
}

resource "aws_eip" "nessus-scanner-eip" {
  vpc      = true
  instance = aws_instance.nessus-scanner.id

  tags = var.nessus_tags
}

resource "aws_vpc" "nessus" {
  cidr_block       = "10.2.0.0/22" # 1,024 IPs
  instance_tenancy = "default"
  tags             = var.nessus_tags
}

resource "aws_subnet" "nessus" {
  vpc_id     = aws_vpc.nessus.id
  cidr_block = "10.2.0.0/24" # 256 IPs

  tags       = var.nessus_tags
  depends_on = [aws_vpc.nessus]
}

resource "aws_route_table" "nessus" {
  vpc_id = aws_vpc.nessus.id
  tags   = var.nessus_tags
}

resource "aws_internet_gateway" "nessus" {
  vpc_id     = aws_vpc.nessus.id
  tags       = var.nessus_tags
  depends_on = [aws_vpc.nessus]
}

resource "aws_route" "internet_gateway" {
  route_table_id         = aws_route_table.nessus.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.nessus.id
}

resource "aws_route_table_association" "nessus_subnet" {
  subnet_id      = aws_subnet.nessus.id
  route_table_id = aws_route_table.nessus.id
}

resource "aws_security_group" "nessus" {
  description            = "Security group for the Nessus VM Scanner Server instance"
  name                   = "nessus-scanner"
  revoke_rules_on_delete = false
  tags                   = var.nessus_tags
  vpc_id                 = aws_vpc.nessus.id
  depends_on             = [aws_vpc.nessus]
}

resource "aws_security_group_rule" "nessus-allow-outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.nessus.id
}

resource "aws_security_group_rule" "nessus-ingress-port-22" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.nessus.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "nessus-ingress-port-8834" {
  type              = "ingress"
  from_port         = 8834
  to_port           = 8834
  protocol          = "tcp"
  security_group_id = aws_security_group.nessus.id
  cidr_blocks       = ["0.0.0.0/0"]
}
