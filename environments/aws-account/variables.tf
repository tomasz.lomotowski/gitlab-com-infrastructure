variable "nessus_tags" {
  type = map(string)

  default = {
    Name        = "nessus-scanner"
    Terraform   = "true"
    Environment = "aws-account"
  }
}
