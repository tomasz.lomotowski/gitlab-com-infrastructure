###############
# Route Table
###############

resource "aws_route_table" "packagecloud" {
  vpc_id = aws_vpc.packagecloud.id

  tags = {
    Name        = var.tag_name
    Terraform   = "true"
    Environment = var.environment
  }
}

#########
# Routes
#########

resource "aws_route" "internet_gateway_ipv4" {
  route_table_id         = aws_route_table.packagecloud.id
  destination_cidr_block = var.all_ips_cidr
  gateway_id             = aws_internet_gateway.packagecloud.id
}

resource "aws_route" "internet_gateway_ipv6" {
  route_table_id              = aws_route_table.packagecloud.id
  destination_ipv6_cidr_block = var.all_ipv6_ips_cidr
  gateway_id                  = aws_internet_gateway.packagecloud.id
}

resource "aws_route_table_association" "packagecloud_subnet" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.packagecloud.id
}
