
# Cloudflare Page Rules for ops.gitlab.com
#
# Runbook: TODO
#
# These rules should be ordered from HIGHEST to LOWEST priority. Higher
# value numbers are a higher priority. This will also sort rules in
# a similar way as the Web UI.

resource "cloudflare_page_rule" "ops_gitlab_net_api_page_rule" {
  zone_id  = var.cloudflare_zone_id
  target   = "*ops.${var.cloudflare_zone_name}/api/v4/*"
  priority = 2

  actions {
    browser_check  = "off"
    waf            = "off"
    always_online  = "off"
    security_level = "off"
    cache_level    = "bypass"
  }
}

resource "cloudflare_page_rule" "ops_gitlab_net_page_rule" {
  zone_id  = var.cloudflare_zone_id
  target   = "ops.${var.cloudflare_zone_name}/*"
  priority = 1

  actions {
    browser_check  = "off"
    waf            = "off"
    always_online  = "off"
    security_level = "off"
    cache_level    = "bypass"
  }
}
