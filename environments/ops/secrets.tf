locals {

  secrets = [
    "aptly_ops",
    "bastion_ops",
    "blackbox-exporter_ops",
    "cookbook-gitlab-runner_ops",
    "elasticsearch-exporter_ops",
    "gitlab-consul_ops-client",
    "gitlab-consul_ops-cluster",
    "gitlab-elk_ops",
    "gitlab-okta-asa_ops",
    "gitlab-omnibus-secrets_dev",
    "gitlab-omnibus-secrets_ops",
    "gitlab-prometheus-secrets_ops",
    "gitlab-proxy_ops-nonprod",
    "gitlab-proxy_ops-prod",
    "gitlab-proxy_ops",
    "gitlab-sentry_ops",
    "gitlab-wale_ops",
    "gitlab-walg_ops",
    "prometheus-alertmanager_ops",
    "public-grafana_ops",
    "syslog-client_ops",
  ]

  common_secret_readers = [
    "serviceAccount:terraform@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:k8s-workloads-ro@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
