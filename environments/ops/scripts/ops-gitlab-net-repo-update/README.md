This script is for populating repositories on the ops instance
it creates pull mirrors for every project, and populates CICD vars

How to use:

1. Copy this directory to ops.gitlab.net
1. Create ops-instance.env from ops-instance.env.example;
    1. OPS_PRIVATE_TOKEN is in 1password in the "ops-gitlab-net system account
       for ops.gitlab.net" entry, under the name:
       `"pat: gitlab.com/ops-gitlab-net-rotation-2".`
    1. CI_PRIVATE_KEY can be obtained from the CI/CD variables from any other
       cookbook project.  May be in 1password, but not sure where.
1. Create a list of repo paths that you want to sync, use the `list-projects.sh`
   script and drop the file in /tmp/projects.list.  Or just put the one project
   you want to add in said file
1. sudo to root: `sudo -s`
1. Load the env vars: `. /home/<yourusername>/ops-instance.env`
1. Copy update-ops-gitlab-net-projects.rb to /tmp or somewhere else readable by 'git',
   which your home directory is not (mode 700)
1. Change to that directory
1. Run `update-ops-gitlab-net-projects.rb` on the rails console by running
   `gitlab-rails runner update-ops-gitlab-net-projects.rb`

If you see `Please specify a valid ruby command or the path of a script to run.`,
then the 'git' user can't see the .rb file you're trying to execute, usually
because permissions.  Double check which dir it's in.
