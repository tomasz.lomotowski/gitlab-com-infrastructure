## You must have the following set in your environment to use this script

#  OPS_PRIVATE_TOKEN
#  CI_PRIVATE_KEY

raise "Environment not set, aborting!" unless ENV['OPS_PRIVATE_TOKEN'] && ENV['CI_PRIVATE_KEY']
u = User.find_by(username: 'ops-gitlab-net')
File.open("/tmp/projects.list").each_line do |full_path|
  full_path = full_path.chomp
  group_path = File.dirname(full_path)
  path = File.basename(full_path)
  unless Group.find_by_full_path(group_path)
    puts "Warning, group #{group_path} does not exist! - skipping"
    next
  end

  p = Project.find_by_full_path(full_path)
  unless p  
    p = ::Projects::CreateService.new(u, path: path, visibility_level: Gitlab::VisibilityLevel::INTERNAL, description: "Created by ops automation", namespace_id: Group.find_by_full_path(group_path).id).execute
  end

  p.mirror = true
  p.only_mirror_protected_branches = false
  p.mirror_user = u
  p.mirror_trigger_builds = true
  p.import_url = "https://ops-gitlab-net:#{ENV['OPS_PRIVATE_TOKEN']}@gitlab.com/#{full_path}.git"
  p.variables.build(key: 'CI_PRIVATE_KEY', value: ENV['CI_PRIVATE_KEY']) unless p.variables.find_by(key: 'CI_PRIVATE_KEY')
  p.save
end
