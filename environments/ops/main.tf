## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/ops/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

variable "gitlab_com_zone_id" {
}

variable "gitlab_net_zone_id" {
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.3"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

## Google

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "google-beta" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

##################################
#
#  NAT gateway
#
#################################
module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.3.0"

  log_level        = "ALL"
  nat_ip_count     = 2
  nat_ports_per_vm = 1024
  network_name     = module.network.name
  region           = var.region

  # A NAT instance with this name was initially created for GKE's subnet. Later,
  # it was decided to use 1 Cloud NAT instance per region rather than per
  # subnet, for simplicity. Cloud NAT resources cannot have their name changed
  # without recreation, and 2 NATs cannot overlap in coverage (e.g. 1
  # subnet-specific NAT coexisting with 1 regional NAT). Therefore changing the
  # NAT or router name would cause downtime. This is why in our main
  # environments the Cloud NAT resource is called "gitlab-gke".
  nat_name    = "gitlab-gke"
  router_name = "gitlab-gke"
}

# We want only 1 logs-based metric per project, not per NAT. This way, the
# stackdriver metric (and exported prometheus metric) will have a static name,
# with different NAT instances being identified by the "nat_name" label. Note
# that Stackdriver already applies a "gateway_name" label so we don't need to
# add our own.
resource "google_logging_metric" "errors" {
  name = "nat-errors"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

resource "google_logging_metric" "errors_by_vm" {
  name = "nat-errors-by-vm"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
    labels {
      key        = "vm_name"
      value_type = "STRING"
    }
  }

  label_extractors = {
    "vm_name" = "EXTRACT(jsonPayload.endpoint.vm_name)"
  }
}

resource "google_logging_metric" "translations" {
  name = "nat-translations"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="OK"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

##################################
#
#  Network
#
#################################

module "network" {
  # TODO Migrate this environment to v2.0.0+ (https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7860)
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  project     = var.project
  environment = var.environment
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering_gprd" {
  name         = "peering-gprd"
  network      = var.network_ops
  peer_network = var.network_gprd
}

resource "google_compute_network_peering" "peering_gstg" {
  name         = "peering-gstg"
  network      = var.network_ops
  peer_network = var.network_gstg
}

resource "google_compute_network_peering" "peering_pre" {
  name         = "peering-pre"
  network      = var.network_ops
  peer_network = var.network_pre
}

resource "google_compute_network_peering" "peering_release" {
  name         = "peering-release"
  network      = var.network_ops
  peer_network = var.network_release
}

resource "google_compute_network_peering" "peering_testbed" {
  name         = "peering-testbed"
  network      = var.network_ops
  peer_network = var.network_testbed
}

resource "google_compute_network_peering" "peering_org-ci" {
  name         = "peering-org-ci"
  network      = var.network_ops
  peer_network = var.network_org-ci
}

##################################
#
#  Log Proxy
#
#################################

# log.gprd.gitlab.net
module "prod-proxy-iap" {
  environment          = var.environment
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v4.1.0"
  name                 = "prod-proxy"
  project              = var.project
  region               = var.region
  gitlab_zone          = "gitlab.net."
  backend_service_link = module.prod-proxy.google_compute_backend_service_iap_self_link
  web_ip_fqdn          = "log.gprd.gitlab.net"
  service_ports        = ["9090"]
}

module "prod-proxy" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-prod-proxy]\""
  dns_zone_name             = var.dns_zone_name
  enable_iap                = true
  environment               = var.environment
  health_check              = "http"
  ip_cidr_range             = var.subnetworks["prod-proxy"]
  machine_type              = var.machine_types["proxy"]
  name                      = "prod-proxy"
  node_count                = 1
  oauth2_client_id          = var.oauth2_client_id_log_proxy
  oauth2_client_secret      = var.oauth2_client_secret_log_proxy
  project                   = var.project
  public_ports              = var.public_ports["proxy"]
  region                    = var.region
  service_account_email     = var.service_account_email
  service_port              = "9090"
  service_path              = "/app/kibana"
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.0.0"
  tier                      = "inf"
  timeout_sec               = 300
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

# nonprod-log.gitlab.net
module "nonprod-proxy-iap" {
  environment          = var.environment
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v4.1.0"
  name                 = "nonprod-proxy"
  project              = var.project
  region               = var.region
  gitlab_zone          = "gitlab.net."
  backend_service_link = module.nonprod-proxy.google_compute_backend_service_iap_self_link
  web_ip_fqdn          = "nonprod-log.gitlab.net"
  service_ports        = ["9090"]
}

module "nonprod-proxy" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-nonprod-proxy]\""
  dns_zone_name             = var.dns_zone_name
  enable_iap                = true
  environment               = var.environment
  health_check              = "http"
  ip_cidr_range             = var.subnetworks["nonprod-proxy"]
  machine_type              = var.machine_types["proxy"]
  name                      = "nonprod-proxy"
  node_count                = 1
  oauth2_client_id          = var.oauth2_client_id_log_proxy
  oauth2_client_secret      = var.oauth2_client_secret_log_proxy
  project                   = var.project
  public_ports              = var.public_ports["proxy"]
  region                    = var.region
  service_account_email     = var.service_account_email
  service_port              = "9090"
  service_path              = "/app/kibana"
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.0.0"
  tier                      = "inf"
  timeout_sec               = 300
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

# log.gitlab.net
module "proxy-iap" {
  environment          = var.environment
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v4.1.0"
  name                 = "proxy"
  project              = var.project
  region               = var.region
  gitlab_zone          = "gitlab.net."
  backend_service_link = module.proxy.google_compute_backend_service_iap_self_link
  web_ip_fqdn          = "log.gitlab.net"
  service_ports        = ["443", "80", "9090"]
}

module "proxy" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-proxy]\""
  dns_zone_name             = var.dns_zone_name
  enable_iap                = true
  environment               = var.environment
  health_check              = "tcp"
  ip_cidr_range             = var.subnetworks["proxy"]
  machine_type              = var.machine_types["proxy"]
  name                      = "proxy"
  node_count                = 1
  oauth2_client_id          = var.oauth2_client_id_log_proxy
  oauth2_client_secret      = var.oauth2_client_secret_log_proxy
  project                   = var.project
  public_ports              = var.public_ports["proxy"]
  region                    = var.region
  service_account_email     = var.service_account_email
  service_port              = "9090"
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.0.0"
  tier                      = "inf"
  timeout_sec               = 300
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  PubSub
#
##################################

module "pubsub" {
  source                = "../../modules/pubsub"
  environment           = var.environment
  project               = var.project
  pubsub_topics         = var.pubsub_topics
  pubsub_filters        = var.pubsub_filters
  service_account_email = var.service_account_email
}

##################################
#
#  Pubsubbeats
#
##################################

## GKE Workload Identity config for beats in kubernetes

# Create a GCP service account
resource "google_service_account" "pubsubbeat-k8s-sa" {
  account_id   = "pubsubbeat-k8s"
  display_name = "pubsubbeat-k8s"
  description  = "Service account used by pubsubbeat in k8s for subscribing to PubSub"
}

# Bind a k8s service account pubsubbeat/es-diagnostics to a GCP service account
resource "google_service_account_iam_member" "pubsubbeat-pubsubbeat" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.pubsubbeat-k8s-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[pubsubbeat/pubsubbeat]"
}

# Give the GCP service account relevant PubSub permissions (assign roles)
resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-subscriber" {
  project = var.project
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "gitlab.net"
  dns_zone_prefix = "ops."
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size        = 50
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "prometheus",
      "port" : element(
        var.monitoring_hosts["ports"],
        index(var.monitoring_hosts["names"], "prometheus"),
      ),
      "health_check_path" : "/graph",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
}

# Exclusive to "ops" environment, do not copy-pasta to others.
module "thanos-rule" {
  allow_stopping_for_update = true
  bootstrap_version         = "6"
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-rule]\""
  data_disk_size            = 100
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-rule"]
  machine_type              = var.machine_types["thanos-rule"]
  name                      = "thanos-rule"
  node_count                = var.node_count["thanos-rule"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.3"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

resource "google_compute_subnetwork" "internal_http_lbs" {
  provider = google-beta

  ip_cidr_range = "10.250.13.0/24"
  name          = "internal-http-lbs"
  network       = module.network.self_link
  purpose       = "INTERNAL_HTTPS_LOAD_BALANCER"
  region        = var.region
  role          = "ACTIVE"
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = 6
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "blackbox" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-blackbox]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  machine_type          = var.machine_types["blackbox"]
  name                  = "blackbox"
  node_count            = var.node_count["blackbox"]
  project               = var.project
  public_ports          = var.public_ports["blackbox"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name       = google_compute_subnetwork.monitoring.name
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  use_external_ip       = true
}

resource "google_compute_subnetwork" "thanos_store" {
  ip_cidr_range            = var.subnetworks["thanos-store"]
  name                     = format("thanos-store-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

module "thanos-store-global" {
  allow_stopping_for_update = true
  bootstrap_version         = "6"
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-store-global]\""
  data_disk_size            = 100
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  ip_cidr_range             = google_compute_subnetwork.thanos_store.ip_cidr_range
  machine_type              = var.machine_types["thanos-store-global"]
  name                      = "thanos-store-global"
  node_count                = var.node_count["thanos-store-global"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.3"
  subnetwork_name           = google_compute_subnetwork.thanos_store.name
  static_private_ip         = false
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "thanos-compact" {
  allow_stopping_for_update = true
  bootstrap_version         = "6"
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-compact]\""
  data_disk_size            = 100
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-compact"]
  machine_type              = var.machine_types["thanos-compact"]
  name                      = "thanos-compact"
  node_count                = var.node_count["thanos-compact"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.3"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

#######################################################
#
# Tenable.IO local Nessus scanner
#
#######################################################

module "nessus" {
  bootstrap_version     = 8
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-nessus]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  machine_type          = var.machine_types["nessus"]
  name                  = "nessus"
  node_count            = var.node_count["nessus"]
  project               = var.project
  public_ports          = var.public_ports["nessus"]
  region                = var.region
  service_account_email = var.service_account_email
  os_disk_size          = 100
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name       = google_compute_subnetwork.monitoring.name
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  use_external_ip       = true
}

#######################################################
#
# Load balancer and VM for dashboards.gitlab.net
#
#######################################################

module "dashboards-internal" {
  environment          = var.environment
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v4.1.0"
  name                 = "dashboards"
  project              = var.project
  region               = var.region
  create_http_forward  = "true"
  gitlab_zone          = "gitlab.net."
  backend_service_link = module.dashboards.google_compute_backend_service_self_link[0]
  web_ip_fqdn          = "dashboards.gitlab.net"
  service_ports        = ["80", "443", "3000"]
}

module "dashboards" {
  allow_stopping_for_update = true
  backend_timeout           = 300
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-dashboards]\""
  data_disk_size            = 150
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  health_check              = "http"
  ip_cidr_range             = var.subnetworks["dashboards"]
  machine_type              = var.machine_types["dashboards"]
  name                      = "dashboards"
  node_count                = 1
  persistent_disk_path      = "/var/lib/grafana"
  project                   = var.project
  region                    = var.region
  service_account_email     = var.service_account_email
  service_ports = [
    {
      "name" : "dashboards",
      "port" : 80,
      "health_check_port" : 3000,
      "health_check_path" : "/api/health",
      "iap" : false,
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

#######################################################
#
# Load balancer and VM for dashboards.gitlab.com
#
#######################################################

module "dashboards-com-lb" {
  environment          = var.environment
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v4.1.0"
  name                 = "dashboards-com"
  project              = var.project
  region               = var.region
  create_http_forward  = "true"
  gitlab_zone          = "gitlab.com."
  backend_service_link = module.dashboards-com.google_compute_backend_service_self_link[0]
  web_ip_fqdn          = "dashboards.gitlab.com"
  service_ports        = ["80", "443", "3000"]
}

module "prometheus-dogfood-lb" {
  environment          = var.environment
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v4.1.0"
  name                 = "prometheus-dogfood"
  project              = var.project
  region               = var.region
  create_http_forward  = "true"
  gitlab_zone          = "gitlab.net."
  tags                 = ["dashboards-com"]
  backend_service_link = module.dashboards-com.google_compute_backend_service_self_link[1]
  web_ip_fqdn          = "prometheus-dogfood.ops.gitlab.net"
  service_ports        = ["9095", "9195"]
  firewall_allow_subnets = concat(
    # GCP NAT IPs - gstg.
    [
      "34.73.36.13/32",
      "34.74.10.170/32",
      "34.74.239.255/32",
      "34.75.191.178/32",
      "34.75.208.82/32",
      "34.75.212.232/32",
      "34.75.216.59/32",
      "34.75.225.32/32",
      "35.227.94.233/32",
      "35.231.53.184/32",
      "35.231.92.80/32",
      "35.237.0.58/32",
      "35.237.47.224/32",
      "35.237.93.20/32",
      "104.196.21.58/32",
      "104.196.221.58/32",
    ],
    # GCP NAT IPs - gprd.
    ["34.74.90.64/28"],
    formatlist("%s/32", module.gitlab-ops.instances.*.network_interface.0.access_config.0.nat_ip),
  )
}

module "dashboards-com" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-public-dashboards]\""
  data_disk_size            = 100
  data_disk_type            = "pd-standard"
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  oauth2_client_id          = var.oauth2_client_id_prometheus_dogfood
  oauth2_client_secret      = var.oauth2_client_secret_prometheus_dogfood
  ip_cidr_range             = var.subnetworks["dashboards-com"]
  machine_type              = var.machine_types["dashboards-com"]
  name                      = "dashboards-com"
  node_count                = 1
  persistent_disk_path      = "/var/lib/grafana"
  project                   = var.project
  region                    = var.region
  service_account_email     = var.service_account_email
  service_ports = [
    {
      "name" : "dashboards-com",
      "port" : 80,
      "health_check_port" : 3000,
      "health_check_path" : "/api/health",
      "iap" : false,
    },
    {
      "name" : "trickster",
      "port" : 9095,
      "health_check_port" : 9195,
      "health_check_path" : "/metrics",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

#######################################################
#
# VM for ops.gitlab.net
#
#######################################################

module "ops-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "ops.gitlab.net." = {
      ttl     = "300"
      records = module.gitlab-ops.instances.*.network_interface.0.access_config.0.nat_ip
      spectrum_config = {
        proxy_protocol = "off",
        ip_firewall    = true,
        ports = {
          "22"  = "2222",
          "80"  = "http",
          "443" = "https"
        }
      }
    },
    "registry.ops.gitlab.net." = {
      ttl     = "300"
      records = module.gitlab-ops.instances.*.network_interface.0.access_config.0.nat_ip
    }
  }
}

module "gitlab-ops" {
  backend_protocol      = "HTTPS"
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-gitlab-primary]\""
  data_disk_size        = 5000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["gitlab-ops"]
  log_disk_size         = 100
  machine_type          = var.machine_types["gitlab-ops"]
  name                  = "gitlab"
  node_count            = 1
  oauth2_client_id      = var.oauth2_client_id_gitlab_ops
  oauth2_client_secret  = var.oauth2_client_secret_gitlab_ops
  persistent_disk_path  = "/var/opt/gitlab"
  project               = var.project
  public_ports          = var.public_ports["gitlab-ops"]
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "gitlab",
      "port" : 443,
      "health_check_port" : 8887
      "health_check_path" : "/-/liveness",
    },
  ]

  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

#######################################################
#
# Database
#
#######################################################

module "ops-db" {
  database_version = "POSTGRES_12"
  disk_autoresize  = true
  name             = "gitlab-ops"
  project          = var.project
  region           = var.region
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.3.1"
  tier             = "db-custom-4-15360"
  vpc              = module.network.self_link

  backup_configuration = {
    enabled = true
  }

  ip_configuration = {
    private_network = module.network.self_link
  }
}

#######################################################
#
# VM and services for us-central1 ops.gitlab.net
#
#######################################################

provider "google" {
  alias   = "us-central"
  project = var.project
  region  = "us-central1"
  version = "~> 3.44"
}

###############################################
#
# Load balancer and VM for the ops bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Consul
#
##################################

module "consul" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["consul"]
  machine_type          = var.machine_types["consul"]
  name                  = "consul"
  node_count            = var.node_count["consul"]
  project               = var.project
  public_ports          = var.public_ports["consul"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 8300
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner
#
##################################

module "runner" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-build]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner"]
  machine_type          = var.machine_types["runner-build"]
  name                  = "runner"
  node_count            = var.node_count["runner"]
  os_disk_size          = 500
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner for ChatOps
#
##################################

module "runner-chatops" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-chatops]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-chatops"]
  machine_type          = var.machine_types["runner-chatops"]
  name                  = "runner-chatops"
  node_count            = var.node_count["runner"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner for Release
#
##################################

module "runner-release" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-release]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-release"]
  machine_type          = var.machine_types["runner-release"]
  name                  = "runner-release"
  node_count            = var.node_count["runner"]
  os_disk_size          = 300
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner for drive snapshot creation and restoring
#
##################################

module "runner-snapshots" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-snapshots]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-snapshots"]
  machine_type          = var.machine_types["runner-snapshots"]
  name                  = "runner-snapshots"
  node_count            = var.node_count["runner"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Sentry
#
##################################

module "sentry-lb" {
  environment                = var.environment
  forwarding_port_ranges     = var.tcp_lbs_sentry["forwarding_port_ranges"]
  fqdns                      = ["sentry.gitlab.net"]
  gitlab_zone                = "gitlab.net."
  health_check_ports         = var.tcp_lbs_sentry["health_check_ports"]
  health_check_request_paths = var.tcp_lbs_sentry["health_check_request_paths"]
  instances                  = module.sentry.instances_self_link
  lb_count                   = length(var.tcp_lbs_sentry["names"])
  name                       = "ops-gcp-tcp-lb-sentry"
  names                      = var.tcp_lbs_sentry["names"]
  project                    = var.project
  region                     = var.region
  source                     = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                    = ["sentry"]
}

module "sentry" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-sentry]\""
  data_disk_size        = 2500
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["sentry"]
  machine_type          = var.machine_types["sentry"]
  name                  = "sentry"
  node_count            = var.node_count["sentry"]
  os_disk_size          = 100
  persistent_disk_path  = "/var/lib/postgresql"
  project               = var.project
  public_ports          = var.public_ports["sentry"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.3"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Aptly
#
##################################

module "aptly-lb" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_aptly["forwarding_port_ranges"]
  fqdns                  = ["aptly.gitlab.com"]
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_aptly["health_check_ports"]
  instances              = module.aptly.instances_self_link
  lb_count               = length(var.tcp_lbs_aptly["names"])
  name                   = "ops-gcp-tcp-lb-aptly"
  names                  = var.tcp_lbs_aptly["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["aptly"]
}

module "aptly" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[aptly-gitlab-com]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["aptly"]
  machine_type          = var.machine_types["aptly"]
  name                  = "aptly"
  node_count            = 1
  persistent_disk_path  = "/opt"
  project               = var.project
  public_ports          = var.public_ports["aptly"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.3"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Chef
#
##################################

module "chef-lb" {
  providers = {
    google = google.us-central
  }
  environment                = var.environment
  forwarding_port_ranges     = var.tcp_lbs_chef["forwarding_port_ranges"]
  fqdns                      = ["chef.gitlab.com"]
  gitlab_zone                = "gitlab.com."
  health_check_ports         = var.tcp_lbs_chef["health_check_ports"]
  health_check_request_paths = var.tcp_lbs_chef["health_check_request_paths"]
  instances                  = module.chef.instances_self_link
  lb_count                   = length(var.tcp_lbs_chef["names"])
  name                       = "${var.environment}-gcp-tcp-lb-chef"
  names                      = var.tcp_lbs_chef["names"]
  project                    = var.project
  region                     = "us-central1"
  source                     = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                    = ["chef"]
}

module "chef" {
  providers = {
    google = google.us-central
  }
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[ops-infra-chef]\""
  data_disk_size        = 50
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["chef"]
  machine_type          = var.machine_types["chef"]
  name                  = "chef"
  node_count            = var.node_count["chef"]
  os_disk_size          = 50
  persistent_disk_path  = "/var/opt/opscode"
  project               = var.project
  public_ports          = var.public_ports["chef"]
  region                = "us-central1"
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.3"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = var.environment
  artifact_age                      = var.artifact_age
  lfs_object_age                    = var.lfs_object_age
  package_repo_age                  = var.package_repo_age
  upload_age                        = var.upload_age
  storage_log_age                   = var.storage_log_age
  storage_class                     = var.storage_class
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.13.0"
  secrets_viewer_access = [
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}",
    "serviceAccount:${google_service_account.k8s-workloads.email}"
  ]
  project = var.project
}

########################################
#
#  Google storage buckets es-diagnostics
#
########################################

resource "google_service_account" "es-diagnostics-sa" {
  account_id   = "es-diagnostics"
  display_name = "es-diagnostics"
  description  = "Service account used by a script for uploading ES diagnostic data"
}

# log.gprd.gitlab.net
resource "google_storage_bucket" "gprd-es-diagnostics-bucket" {
  name = "gitlab-gprd-es-diagnostics"

  versioning {
    enabled = var.gcs_gprd_es_diagnostics_versioning
  }

  storage_class = var.gcs_gprd_es_diagnostics_storage_class

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-gprd-es-diagnostics"
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age = var.gcs_gprd_es_diagnostics_age
    }
  }

}

resource "google_storage_bucket_iam_binding" "gprd-es-diagnostics-binding" {
  bucket     = "gitlab-gprd-es-diagnostics"
  role       = "roles/storage.objectAdmin"
  depends_on = [google_storage_bucket.gprd-es-diagnostics-bucket]

  members = [
    "serviceAccount:${var.service_account_email}",
    "serviceAccount:${google_service_account.es-diagnostics-sa.email}",
  ]
}

# Updates an IAM policy to add the kubernetes service account es-diagnostics/es-diagnostics to the GCP service account in order to be able to use GKE Workload Identity
resource "google_service_account_iam_member" "es-diagnostics-es-diagnostics" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.es-diagnostics-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[es-diagnostics/es-diagnostics]"
}

# advanced search
resource "google_storage_bucket" "gprd-es-diagnostics-search-bucket" {
  name = "gitlab-gprd-es-diagnostics-search"

  versioning {
    enabled = var.gcs_gprd_es_diagnostics_versioning
  }

  storage_class = var.gcs_gprd_es_diagnostics_storage_class

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-gprd-es-diagnostics-search"
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age = var.gcs_gprd_es_diagnostics_age
    }
  }

}

resource "google_storage_bucket_iam_binding" "gprd-es-diagnostics-search-binding" {
  bucket     = "gitlab-gprd-es-diagnostics-search"
  role       = "roles/storage.objectAdmin"
  depends_on = [google_storage_bucket.gprd-es-diagnostics-search-bucket]

  members = [
    "serviceAccount:${var.service_account_email}",
    "serviceAccount:${google_service_account.es-diagnostics-sa.email}",
  ]
}

resource "google_service_account_iam_member" "es-diagnostics-es-diagnostics-search-prod" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.es-diagnostics-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[es-diagnostics/es-diagnostics-search-prod]"
}

# nonprod-log.gitlab.net
resource "google_storage_bucket" "ops-es-diagnostics-bucket" {
  name = "gitlab-ops-es-diagnostics"

  versioning {
    enabled = var.gcs_ops_es_diagnostics_versioning
  }

  storage_class = var.gcs_ops_es_diagnostics_storage_class

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-ops-es-diagnostics"
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age = var.gcs_ops_es_diagnostics_age
    }
  }

}

resource "google_storage_bucket_iam_binding" "ops-es-diagnostics-binding" {
  bucket     = "gitlab-ops-es-diagnostics"
  role       = "roles/storage.objectAdmin"
  depends_on = [google_storage_bucket.ops-es-diagnostics-bucket]

  members = [
    "serviceAccount:${var.service_account_email}",
    "serviceAccount:${google_service_account.es-diagnostics-sa.email}",
  ]
}

resource "google_service_account_iam_member" "es-diagnostics-es-diagnostics-nonprod" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.es-diagnostics-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[es-diagnostics/es-diagnostics-nonprod]"
}

##################################
#
#  Sentry bucket
#
##################################

resource "google_storage_bucket" "gitlab-sentry" {
  name = "gitlab-sentry"

  versioning {
    enabled = "false"
  }

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-sentry"
  }
}

resource "google_service_account" "gitlab-sentry" {
  account_id   = "gitlab-sentry"
  display_name = "gitlab-sentry"
}

resource "google_storage_bucket_iam_binding" "gitlab-sentry" {
  bucket = google_storage_bucket.gitlab-sentry.name
  role   = "roles/storage.objectAdmin"

  members = [
    format("serviceAccount:%s", google_service_account.gitlab-sentry.email),
  ]
}

##################################
#
#  GitLab Billing bucket
#
##################################

resource "google_storage_bucket" "gitlab-billing" {
  name = "gitlab-billing"

  versioning {
    enabled = "false"
  }

  storage_class = "NEARLINE"

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-billing"
  }
}

resource "google_storage_bucket_iam_binding" "billing-viewer-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.objectViewer"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
  ]
}

resource "google_storage_bucket_iam_binding" "billing-legacy-bucket-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.legacyBucketReader"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
    "projectViewer:gitlab-ops",
  ]
}

resource "google_storage_bucket_iam_binding" "billing-legacy-object-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.legacyObjectReader"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
  ]
}

##################################
#
#  GKE Cluster for ops
#
##################################

# Service account utilized by CI
resource "google_service_account" "k8s-workloads" {
  account_id   = "k8s-workloads"
  display_name = "k8s-workloads"
}

# Service account utilized by CI for Read Only operations
resource "google_service_account" "k8s-workloads-ro" {
  account_id   = "k8s-workloads-ro"
  display_name = "k8s-workloads-ro"
}

resource "google_project_iam_custom_role" "k8s-workloads" {
  project     = var.project
  role_id     = "k8sWorkloads"
  title       = "k8s-workloads"
  permissions = ["clientauthconfig.clients.listWithSecrets", "container.secrets.list"]
}

resource "google_project_iam_binding" "k8s-workloads" {
  project = var.project
  role    = "projects/${var.project}/roles/${google_project_iam_custom_role.k8s-workloads.role_id}"

  members = [
    "serviceAccount:${google_service_account.k8s-workloads.email}",
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}"
  ]
}

# Reserved IP address for prometheus operator
resource "google_compute_address" "prometheus-gke" {
  name         = "prometheus-gke-${var.environment}"
  description  = "prometheus-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for alertmanager ingress with IAP (GKE)
resource "google_compute_global_address" "alertmanager-ingress-iap" {
  name        = "alertmanager-ingress-iap-${var.environment}"
  description = "alertmanager-ingress-iap-${var.environment}"
}

module "alertmanager-gke-ops-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "alerts.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.alertmanager-ingress-iap.address]
    }
  }
}

# Reserved IP address for prometheus ingress with IAP
resource "google_compute_global_address" "prometheus-ingress-iap" {
  name        = "prometheus-ingress-iap-${var.environment}"
  description = "prometheus-ingress-iap-${var.environment}"
}

module "prometheus-gke-ops-gitlab-net-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "prometheus-gke.ops.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.prometheus-ingress-iap.address]
    }
  }
}

# Reserved IP address for thanos-query
resource "google_compute_address" "thanos-query-gke" {
  name         = "thanos-query-gke-${var.environment}"
  description  = "thanos-query-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

resource "google_compute_global_address" "woodhouse" {
  name        = "woodhouse-${var.environment}"
  description = "IP for Woodhouse's GKE ingress, annotation-bound in tanka."
}

module "woodhouse_dns_record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "woodhouse.${var.environment}.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.woodhouse.address]
    }
  }
}

resource "google_compute_global_address" "thanos_query_frontend" {
  name        = "thanos-query-frontend-${var.environment}"
  description = "IP for thanos-query-frontend's GKE ingress, annotation-bound in tanka."
}

module "thanos_query_frontend_dns_record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "thanos.gitlab.net." = {
      ttl     = "300"
      records = [google_compute_global_address.thanos_query_frontend.address]
    }
  }
}

module "thanos_query_frontend_dns_record_alt" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  cname = {
    "thanos-query.ops.gitlab.net." = {
      ttl     = "300"
      records = ["thanos.gitlab.net."]
    }
  }
}

module "gitlab-gke" {
  name        = "gitlab-gke"
  environment = var.environment
  vpc         = module.network.self_link
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v11.3.0"

  authorized_master_access = [
    "35.229.103.65/32",  # console-01-sv-gprd
    "35.227.116.144/32", # ops.gitlab.net
    "35.231.50.113/32",  # runner-release-01-inf-ops
    "35.185.18.176/32",  # runner-01-inf-ops
    "35.237.45.18/32",   # bastion-01-inf-ops
  ]

  ip_cidr_range          = var.subnetworks["gitlab-gke"]
  disable_network_policy = "false"
  dns_zone_name          = var.dns_zone_name
  kubernetes_version     = "1.18"
  maintenance_policy = {
    start_time = "2020-09-14T02:00:00Z"
    end_time   = "2020-09-14T08:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=MO,TU"
  }
  nodes_service_account_name = "gitlab-gke-nodes"
  private_master_cidr        = var.master_cidr_subnets["gitlab-gke"]
  project                    = var.project
  region                     = var.region
  release_channel            = "REGULAR"
  pod_ip_cidr_range          = var.subnetworks["gitlab-gke-pod-cidr"]
  service_ip_cidr_range      = var.subnetworks["gitlab-gke-service-cidr"]
  subnet_nat_name            = "gitlab-gke-ops"

  service_account = "default"

  upgrade_notification_queue = google_pubsub_topic.gke_notifications.id

  node_pools = {
    "default-20200401-0" = {
      machine_type      = var.machine_types["gitlab-gke"]
      max_node_count    = "9"
      node_auto_upgrade = true
      type              = "default"
    }
  }
}

// Pubsub topic for gke-notificationsj
resource "google_pubsub_topic" "gke_notifications" {
  name = "gke_notifications"
}

// Cloud function to get upgrade notifications/annotations from gke
resource "google_cloudfunctions_function" "gke_notifications" {
  entry_point = "HelloPubSub"
  environment_variables = {
    ENVIRONMENT     = "ops",
    GRAFANA_API_KEY = var.grafana_api_key,
    GRAFANA_URL     = "https://dashboards.gitlab.net/api/annotations",
  }
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.gke_notifications.id
  }
  name                  = "gke_notifications"
  project               = var.project
  region                = var.region
  runtime               = "go113"
  source_archive_bucket = "gitlab-gke-notifications-function"
  source_archive_object = "gke-notifications.zip"
}

// Allow GKE to talk to the prometheus operator which utilizes port 8443
resource "google_compute_firewall" "gke-master-to-kubelet" {
  name    = "k8s-api-to-kubelets"
  network = module.network.self_link
  project = var.project

  description = "GKE API to kubelets"

  source_ranges = [var.master_cidr_subnets["gitlab-gke"]]

  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }

  target_tags = ["gitlab-gke"]
}

##################################
#
#  Kubernetes external-dns
#
##################################

# Note that the resources in this block duplicate code in
# `../../modules/gke-external-dns`. There were difficulties performing a
# terraform state migration, that are preventing us from DRYing out this code
# for now. See
# https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/merge_requests/1967
# for details.

resource "google_dns_managed_zone" "kube_external_dns" {
  name     = "kube-external-dns-${var.environment}"
  dns_name = "${var.environment}.gke.gitlab.net."
}

module "kube_external_dns_zone_delegation" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."
  ns = {
    "${var.environment}.gke.gitlab.net." = {
      ttl     = "3600"
      records = google_dns_managed_zone.kube_external_dns.name_servers
    }
  }
}

# Used by workload identity, no explicit private key
resource "google_service_account" "kube_external_dns" {
  account_id  = "kube-external-dns-${var.environment}"
  description = "A service account for use by this environments's GKE cluster's external-dns deployment."
}

resource "google_project_iam_member" "kube_external_dns" {
  role   = "roles/dns.admin"
  member = "serviceAccount:${google_service_account.kube_external_dns.email}"
}

resource "google_project_iam_member" "kube_external_dns_permit_workload_identity" {
  role   = "roles/iam.workloadIdentityUser"
  member = "serviceAccount:${var.project}.svc.id.goog[external-dns/external-dns]"
}

##################################
#
#  GCS Bucket for postgres backup
#
##################################

module "postgres-backup" {
  environment    = var.environment
  backup_readers = ["serviceAccount:${var.gcs_postgres_restore_service_account}"]
  backup_writers = ["serviceAccount:${var.gcs_postgres_backup_service_account}"]
  kms_key_id     = var.gcs_postgres_backup_kms_key_id
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/database-backup-bucket.git?ref=v4.3.0"
}

##################################
#
#  Service accounts
#
##################################

## Service account used for granting ops
## write access to asset buckets

resource "google_service_account" "assets" {
  account_id   = "asset-uploader"
  display_name = "Service account that allows ops to write to assets buckets in other projects"
}

##################################
#
#  Outputs
#
##################################

output "ops_ip" {
  value = module.gitlab-ops.instances.0.network_interface.0.access_config.0.nat_ip
}

############
# cloudflare
############

resource "google_storage_bucket" "ops_cloudflare_logs" {
  name          = "gitlab-ops-cloudflare-logpush"
  location      = "US"
  project       = var.project
  force_destroy = true

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-ops-cloudflare-logpush"
  }

  lifecycle_rule {
    condition {
      age = "91"
    }
    action {
      type = "Delete"
    }
  }
}

resource "google_storage_bucket_iam_member" "logpush_cloudflare" {
  bucket = google_storage_bucket.ops_cloudflare_logs.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:logpush@cloudflare-data.iam.gserviceaccount.com"
}

resource "cloudflare_zone_settings_override" "gitlab_net_settings_override" {

  zone_id = var.cloudflare_zone_id
  settings {
    # We have this limit set to 500 for ops as we don't need larger, and we can't specify larger here without cloudflare changing settings
    max_upload                  = 500
    waf                         = "on"
    always_use_https            = "on"
    min_tls_version             = "1.2"
    ssl                         = "origin_pull"
    tls_1_3                     = "on"
    brotli                      = "on"
    sort_query_string_for_cache = "on"
    http2                       = "on"
    ipv6                        = "on"
    websockets                  = "on"
    opportunistic_onion         = "on"
    pseudo_ipv4                 = "off"
  }
}

resource "null_resource" "manual_cloudflare_settings" {

  provisioner "local-exec" {
    command = <<EOF
      echo '
      # 2020-03-23 Contacted cloudflare to increase limit to 5GiB https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9167
      # 2020-03-13 Contacted cloudflare to increase timeout to 400 seconds https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/8475'
      EOF
  }

}

resource "cloudflare_argo" "gitlab_net_argo" {
  zone_id        = var.cloudflare_zone_id
  tiered_caching = "on"
  smart_routing  = "on"
}

module "internal-grafana-db" {
  availability_type               = "REGIONAL"
  database_version                = "POSTGRES_11"
  maintenance_window_day          = 6  # Saturday
  maintenance_window_hour         = 17 # UTC
  maintenance_window_update_track = "stable"
  name                            = "internal-grafana"
  user_name                       = "grafana"
  db_name                         = "grafana"
  project                         = var.project
  region                          = var.region
  source                          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.3.1"
  tier                            = var.machine_types["internal-grafana-db"]
  vpc                             = module.network.self_link

  ip_configuration = {
    private_network = module.network.self_link
  }

  backup_configuration = {
    enabled = "true"
  }
}

module "public-grafana-db" {
  availability_type               = "REGIONAL"
  database_version                = "POSTGRES_11"
  maintenance_window_day          = 6  # Saturday
  maintenance_window_hour         = 17 # UTC
  maintenance_window_update_track = "stable"
  name                            = "public-grafana"
  project                         = var.project
  region                          = var.region
  user_name                       = "grafana"
  db_name                         = "grafana"
  source                          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.3.1"
  tier                            = var.machine_types["public-grafana-db"]
  vpc                             = module.network.self_link

  ip_configuration = {
    private_network = module.network.self_link
  }

  backup_configuration = {
    enabled = "true"
  }
}

##################################
#
#  CI Peering related firewall rules
#
##################################

// Disable inbound from org-ci IP ranges
resource "google_compute_firewall" "block-org-ci-network" {
  name    = "block-org-ci-network"
  network = module.network.self_link
  project = var.project

  description = "Block traffic from org-ci subnets"

  source_ranges = var.org_ci_blocked_ranges

  deny {
    protocol = "all"
  }
}

##################################
#
#  Google storage buckets for GitLab team-members
#
##################################

# https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10637
resource "google_storage_bucket" "gitlab_cov_fuzz" {
  name     = "gitlab-cov-fuzz"
  location = "US"
  project  = var.project
}
# Make bucket contents public
resource "google_storage_bucket_iam_member" "gitlab_cov_fuzz_read" {
  bucket = google_storage_bucket.gitlab_cov_fuzz.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}
# Create service account for read/write access
resource "google_service_account" "gitlab_cov_fuzz" {
  account_id   = "gitlab-cov-fuzz"
  display_name = "gitlab-cov-fuzz"
  description  = "Service account used to publish binaries to the gitlab-cov-fuzz bucket"
}
resource "google_storage_bucket_iam_member" "gitlab-cov-fuzz-write" {
  bucket = google_storage_bucket.gitlab_cov_fuzz.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.gitlab_cov_fuzz.email}"
}
# End Google storage buckets for GitLab team-members

###
#
# Storage bucket configuration for gke-notifications cloud function
# https://gitlab.com/gitlab-com/gl-infra/gke-notifications
#
###
resource "google_storage_bucket" "gke_notifications_code" {
  name     = "gitlab-gke-notifications-function"
  location = "US"
  project  = var.project
}
# Make bucket contents public
resource "google_storage_bucket_iam_member" "gke_notifications_code" {
  bucket = google_storage_bucket.gke_notifications_code.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}
# Create service account for read/write access
resource "google_service_account" "gke_notifications_code" {
  account_id   = "gitlab-gke-notifications"
  display_name = "gitlab-gke-notifications"
  description  = "Service account used to publish code for gke-notifications repo https://gitlab.com/gitlab-com/gl-infra/gke-notifications"
}
resource "google_storage_bucket_iam_member" "gitlab_gke-notifications" {
  bucket = google_storage_bucket.gke_notifications_code.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.gke_notifications_code.email}"
}
