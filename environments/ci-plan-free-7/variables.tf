variable "project" {
  default = "gitlab-ci-plan-free-7-7fe256"
}

variable "network" {
  default = "ephemeral-runners"
}
