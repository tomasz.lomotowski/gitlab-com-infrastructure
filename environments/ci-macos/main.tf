terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/ci-macos/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

##################################
#
#  Service Accounts
#
##################################

# Developers/SRE are added to this group.
resource "google_project_iam_member" "gcp-ci-ops" {
  role   = "roles/owner"
  member = "group:gcp-ci-ops-sg@gitlab.com"
}
