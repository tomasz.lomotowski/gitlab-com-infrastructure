variable "project" {
  default = "gitlab-ci-macos-58e4e0"
}

variable "region" {
  default = "us-east1"
}
