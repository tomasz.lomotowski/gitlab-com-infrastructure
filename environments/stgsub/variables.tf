variable "environment" {
  default = "stgsub"
}

variable "project" {
  default = "gitlab-subscriptions-staging"
}

variable "region" {
  default = "us-east1"
}

variable "service_account_email" {
  default = "560894963203-compute@developer.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-subscriptions-staging.iam.gserviceaccount.com"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

variable "cloudflare_api_key" {}

variable "cloudflare_account_id" {}

variable "cloudflare_email" {}

variable "gitlab_com_zone_id" {}

variable "bootstrap_script_version" {
  default = 8
}

variable "dns_zone_name" {
  default = "staging.gitlab.com"
}

variable "subnetworks" {
  type = map(string)

  default = {
    "customers" = "10.0.2.0/24"
    "bastion"   = "10.0.3.0/24"
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "customers" = [80, 443]
    "bastion"   = [22]
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "customers" = 1
    "bastion"   = 1
  }
}

variable "machine_types" {
  type = map(string)

  default = {
    "customers" = "n1-standard-4"
    "bastion"   = "n1-standard-1"
  }
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-subscriptions-bastion.staging.gitlab.com"]
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-stgsub-chef-bootstrap"
    bootstrap_key     = "gitlab-stgsub-bootstrap-validation"
    bootstrap_keyring = "gitlab-stgsub-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}
