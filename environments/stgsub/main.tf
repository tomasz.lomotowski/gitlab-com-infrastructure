## State Storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/stg-subscriptions/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.42"
}

provider "google-beta" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "cloudflare" {
  version    = "= 2.5.1"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

module "storage" {
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.13.0"
  environment                       = var.environment
  project                           = var.project
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  prometheus_workload_identity      = false
}

### START A service account for ansible dynamic inventory
resource "google_service_account" "ansible_inventory" {
  account_id   = "ansible-inventory"
  display_name = "Ansible Inventory Service Account"
}

resource "google_project_iam_binding" "ansible_inventory" {
  project = var.project
  role    = "roles/compute.viewer"

  members = [
    "serviceAccount:${google_service_account.ansible_inventory.email}",
  ]
}
### END A service account for ansible dynamic inventory


resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = google_kms_key_ring.bootstrap.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_crypto_key" "bootstrap-validation" {
  name            = "gitlab-${var.environment}-bootstrap-validation"
  key_ring        = google_kms_key_ring.bootstrap.id
  rotation_period = "7776000s" # 90 days

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_kms_key_ring" "secrets" {
  name     = "gitlab-secrets"
  location = "global"
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = google_kms_key_ring.secrets.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_crypto_key" "secrets" {
  name            = var.environment
  key_ring        = google_kms_key_ring.secrets.id
  rotation_period = "7776000s" # 90 days

  lifecycle {
    prevent_destroy = true
  }
}

module "network" {
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  project     = var.project
  environment = var.environment
}

## Bastion Nodes
module "bastion" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "bastion-tcp-lb" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "staging.gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

## Subscriptions App Nodes (customers)
module "customers" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-customers]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["customers"]
  machine_type          = var.machine_types["customers"]
  name                  = "customers"
  node_count            = var.node_count["customers"]
  project               = var.project
  public_ports          = var.public_ports["customers"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

## Subscriptions URL Map
resource "google_compute_url_map" "customers-url-map" {
  name            = "subscriptions-url-map"
  default_service = module.customers.google_compute_backend_service_self_link

  host_rule {
    hosts        = ["customers.staging.gitlab.com"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = module.customers.google_compute_backend_service_self_link

    path_rule {
      paths   = ["/*"]
      service = module.customers.google_compute_backend_service_self_link
    }
  }
}

## Subscriptions HTTPS Load Balancer
module "customers-https-lb" {
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  region          = var.region
  project         = var.project
  subnetwork_name = var.subnetworks["customers"]
  name            = "customers"
  targets         = ["customers"]
  hosts           = ["customers"]
  dns_zone_name   = var.dns_zone_name
  dns_zone_id     = var.gitlab_com_zone_id
  environment     = var.environment
  service_ports   = var.public_ports["customers"]
  url_map         = google_compute_url_map.customers-url-map.self_link
}
