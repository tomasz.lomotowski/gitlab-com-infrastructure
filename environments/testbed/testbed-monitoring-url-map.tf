###########################################################
# This is specific to the testbed environment
# and defines the mapping from monitoring hosts to backend
# services

resource "google_compute_url_map" "monitoring-lb" {
  name            = format("%v-monitoring-lb", var.environment)
  default_service = module.prometheus.google_compute_backend_service_self_link[0]

  host_rule {
    hosts        = ["prometheus.testbed.gitlab.net"]
    path_matcher = "prometheus"
  }

  path_matcher {
    name            = "prometheus"
    default_service = module.prometheus.google_compute_backend_service_self_link[0]

    path_rule {
      paths   = ["/*"]
      service = module.prometheus.google_compute_backend_service_self_link[0]
    }
  }
}

