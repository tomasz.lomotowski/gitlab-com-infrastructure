locals {

  secrets = [
    "bastion_testbed",
    "gitlab-omnibus-secrets_testbed",
    "gitlab-prometheus-secrets_testbed",
    "syslog-client_testbed",
  ]

  common_secret_readers = [
    "serviceAccount:terraform@${var.project}.iam.gserviceaccount.com",
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
