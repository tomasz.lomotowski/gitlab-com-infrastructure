# GitLab.com Infrastructure

## What is this?

Here you can find the Terraform configuration for GitLab.com, the staging environment and possibly something else.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## How the repository is structured

Each environment has its own directory.

There is currently one [Terraform state](https://www.terraform.io/docs/state/index.html) for each environment although we're considering to split at least the production one into smaller ones.

Cattle is handled in loops while pets require the full configuration to be
coded. This means you only need to change `count` in `main.tf` to scale a fleet
in and out. More information about [managing cattle can be found in our
docs](./docs/working-with-cattle.md)

### env-projects

All environments in which we have deployed compute resources utilize CI/CD to run `terraform plan`
and manual jobs for `terraform apply`. The exception to this pattern is the meta-environment
`env-projects`, which is used to bootstrap and manage the configuration on all GCP projects with
deployed infrastructure. `env-projects` uses the [project](https://gitlab.com/gitlab-com/gl-infra/terraform-modules/google/project) module to manage configurations for GCP
projects, and terraform runs those operations from the default genesis project, [env-zero](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#env-zero).

### Modules

Modules live in individual repositories in the
[`gitlab-com/gl-infra/terraform-modules`](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules)
group in the ops.gitlab.net instance (see
[infrastructure#5688](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5688)
for current progress).  These standalone modules use automatic semantic
versioning.  See CONTRIBUTING.md in each repo for details; in short, if you
prefix your commit messages with a known magic value (fix:, feat: etc) then when
merged an updated version tag will be pushed based on semver rules, which can
then be used as the ref value of the 'source' attribute of module use in this
repo.

After releasing a new module version, you are encouraged to verify that no
unaccounted for side-effects / traps for the next developer are left in any
environment by bumping the module declaration everywhere, unless there is a good
reason not to. There is a script in `./bin/bump-module` to make this easier.

## Setup (How can I use this?)

First and foremost, remember that Terraform is an extremely powerful tool so be sure you know how to use it well before applying changes in production. Always `plan` and don't hesitate to ask in `#production` if you are in doubt or you see something off in the plan, even the smallest thing.

* [Create a new staging environment](https://drive.google.com/open?id=0BzamLYNnSQa_cjN5NGtaRnpyRXc) (video - internal only)<br>Watch this for a complete overview of how to work with Terraform.

### Making changes

We value small changes.  There are times when we need to make changes across a large number of hosts in the infrastructure.  If possible, break your changes down into smallest subset of functional changes in order to limit the effects of a change at first.  This also helps us keep the master branch `terraform apply`-able.  We have learned from past incidents that it can be hard to untangle big changes.  We may have multiple people working on terraform at the same time so keeping your changes smaller, concise, and more incremental will make all changes easier to incorporate.

We have a badge at the top - before you make a branch off of master, you will want to make sure that TF plan is clean on the environment you plan to change.  This will hopefully help keep you from getting into a situation where the state file gets complicated.

Checkout the [Terraform Broken Master Runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/terraform-broken-master.md) for more details on keeping TF in a clean state.

### Important notes

* We use differing versions of terraform in our environments.  You'll want to
  ensure you have all available versions prior running anything in terraform.
  * `find ./ -name .terraform-version -exec cat {} \;`
  * our wrapper scripts mentioned above will help protect you from running
    terraform with the incorrect desired version
* Make sure you're not specifying `knife[:identity_file]` in your `.chef/knife.rb`. If you do need it then comment it out every time you create a new node. If you don't then the node provisioning will fail with an error like this: `Errno::ENODEV: Operation not supported by device`.
* Be extremely careful when you initialize the state for the first time. Do not load the variables on staging and then `cd` to production to initialize the state: this would link production to the staging state and you do not want that. It's good practice to initialize the states as soon as you set up your environment to avoid making mistakes, so:
```
cd staging
tf init -upgrade
cd ../production
tf init -upgrade
...
```
Then start working.

* DNS is currently managed both on Route53 as well as Cloudflare. If you touch any of the `.auto.tfvars.json` files, before planning (or committing) you need to run `./convert_to_cloudflare.rb` within the `dns` environment to generate the appropriate mapping for the Cloudflare Terraform resources and to make sure it parses correctly.
If you do not do this, terraform will complain, that `var.cloudflare_records` is unset.

#### Getting Started:

  1. Clone [chef-repo](https://ops.gitlab.net/gitlab-cookbooks/chef-repo) if you haven't already.
  1. Install a tool for generating passwords that are easier for human beings to remember, like the [xkcd password generator](https://github.com/redacted/XKCD-password-generator) which may be installed with `pip install xkcdpass`, or [pwgen](https://github.com/jbernard/pwgen), which is available in the Ubuntu, Debian, Fedora, Suse, and macOS Homebrew repositories (`apt install --assume-yes pwgen`, `yum install -y pwgen`, `dnf install -y pwgen`, or `brew install pwgen`).
  1. It is highly suggested to utilize some sort of terraform versioning manager
     such as [asdf](https://github.com/asdf-vm/asdf) in order to manage multiple
     versions of Terraform. `.terraform-version` files are the source of truth
     for what we run in each environment, and it is important not to upgrade the
     minimum version in the tfstate due to premature use of a later terraform
     version.
  1. There is a wrapper script in the `bin/` directory that should be used in place of the `terraform` executable, `tf`. Ensure that `gitlab-com-infrastructure/bin` is in your $PATH
  1. You need to provide some secrets to Terraform using shell env vars. Wrappers in `bin/` source a number of files in `private/env_vars`.
  1. Install the [1password CLI tool](https://1password.com/downloads/command-line/)
  1. Login to 1password with `op signin gitlab user@gitlab.com OP-SECRET-KEY` and `eval $(op signin gitlab)`.
  1. Run `tf-get-secrets` to fetch and update the env var files.
  1. Be sure that the [gcloud-cli is configured and authenticated](https://gitlab.com/gitlab-com/runbooks/blob/master/docs/uncategorized/gcloud-cli.md)
  1. Set up access via [GSTG bastion hosts](https://gitlab.com/gitlab-com/runbooks/blob/master/docs/bastions/gstg-bastions.md)
  1. See Important notes section above about initializing terraform. Run `tf init -upgrade` and `tf plan` in the `environments/gstg` directory and ensure that terraform runs and you can see pending changes
     * :warning: This is a temporary workaround until the [issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/13034) is better understood and properly mitigated.
        * If you experience the error `dial tcp: lookup <domain> on xxx.xxx.xxx.xxx:53: server misbehaving` that means the DNS servers are refusing the queries. Please, add the "offending" domain names to your `/etc/hosts` and try again.
  1. If you're going to be using the Okta ASA env (okta-asa) directly from your laptop (e.g. for testing), you'll need the provider:
     1. git clone git@github.com:splunk/terraform-provider-scaleft.git
     1. cd terraform-provider-scaleft
     1. go build -o terraform-provider-scaleft
     1. mkdir -p ~/.terraform/plugins
     1. cp terraform-provider-scaleft ~/.terraform/plugins/

### Creating a new environment

Bootstrapping new GitLab environments is still not fully automated or defined.
This section concerns itself more with setting up a new terraform-able GCP
environment, which can then be used to provision resources to create a new
GitLab environment.

There are 2 parts to this: we must create the GCP resources, which consist of
the project itself and a service account that can create (and eventually) apply
plans in CI. Secondly, we must create AWS resources to act as a terraform
backend.

These instructions currently jump between a few different documents in order to
satisfy different use cases:

1. Creating brand new environments with a new GCP project.
1. Importing existing GCP projects into terraform management.
1. Adding CI planning/applying for GCP projects that are already managed by
   terraform.

Directions:

1. Follow the instructions in
   [environments/env-projects/README.md](environments/env-projects/README.md) to
   create or import a GCP project.
1. In the production GitLab AWS account, create a new IAM user `terraform-$ENV`
   with programmatic access. Direct attach a new custom policy (named the same
   as the user)  modeled on the existing `terraform-$ENV` users (basically
   `s3:ListBucket` for the `gitlab-com-infrastructure` bucket, and `s3:*` for
   `gitlab-com-infrastructure/terraform/$ENV`, but it is best to copy/paste/edit
   one of the existing policies, changing the path in the second stanza
1. Create a 1password entry "terraform-private/env_vars/$ENV.env" in the
   production vault. It should contain AWS IAM env var declarations for the
   credentials generated in the previous step.
1. In gitlab-com-infrastructure, run `tf-get-secrets` (it's in `./bin`, which is
   always on `$PATH` in order to properly interact with this repo). The new env
   file should be downloaded. You should now be able to run `tf init -upgrade` in
   `environments/$ENV` and plan/apply.
1. Create [CI env
   vars](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/settings/ci_cd)
   for the new environment:

   1. Entries for each var in the `private/env-vars` file, including the AWS
      credentials.
   1. A file entry with key `GCLOUD_TERRAFORM_PRIVATE_KEY_JSON` whose value is
      the contents of the private key file created for the `terraform-ci` user
      previously.

1. Add a section to `.gitlab-ci.yml` for the new environment. Look to existing
   entries for inspiration.
1. CI should now plan (and eventually apply) plans for the new environment.

### Setting up chef bootstrapping secrets

If you plan on running chef-client on your environment you'll also need to
follow these steps:

1. Create a keyring, key and bucket for the bootstrap secrets. Add the following
to your environment:

```
##################################
#
#  Service Accounts
#
##################################

# Create the bootstrap KMS key ring
resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = "${var.project}/global/gitlab-${var.environment}-bootstrap"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_crypto_key" "bootstrap-validation" {
  name            = "gitlab-${var.environment}-bootstrap-validation"
  key_ring        = google_kms_key_ring.bootstrap.id
  rotation_period = "7776000s" # 90 days

  lifecycle {
    prevent_destroy = true
  }
}

##################################
#
#  Object storage buckets
#
##################################

module "gitlab_object_storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.12.2"
  project                           = var.project
}
```

1. Get the chef private key from 1password (item: `gitlab-validator`,
vault: `Production`).

1. Encrypt the chef private key (`validation.pem` is the plaintext file with the
chef private key):

```
gcloud --project gitlab-[ENVIRONMENT] kms encrypt --location global --keyring=gitlab-[ENVIRONMENT]-bootstrap --ciphertext-file=validation.enc --plaintext-file=validation.pem --key=gitlab-[ENVIRONMENT]-bootstrap-validation 1>&2
```

1. Copy the encrypted file to the bucket:

```
gsutil cp validation.enc gs://gitlab-[ENVIRONMENT]-chef-bootstrap
```

### Troubleshooting

- `Identity Pool does not exist ([PROJECT_ID].svc.id.goog)`. You may get this
error on a fresh GCP project. For a workaround, see https://github.com/kubeflow/website/issues/2121#issuecomment-672899088

#### Apply permissions

You may occasionally see permissions errors in the output from the apply jobs if the `terraform-ci`
service account for that project does not have sufficient privileges to create/modify/delete the
resources being modified by your change. To correct this, one of two approaches should be followed:

#### Global changes

If the missing permissions apply to all projects universally, make the change to the default set of
roles assigned to the service account by the project module under
[`local.default_service_account_roles`]()https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project/blob/1ccebcc3ffaf32de81294ad8b99d426295a3636f/variables.tf#L117-121.
Once the variable defaults have been updated, and a new, tagged version of the module has been
[released](https://gitlab.com/gitlab-com/gl-infra/terraform-modules/google/project/blob/master/CONTRIBUTING.md),
use
[`./bin/bump-module`](https://gitlab.com/gitlab-com/gitlab-com-infrastructure/blob/master/bin%2Fbump-module)
in this repository to bump the version of the module referenced in `env-projects`, then manually
apply the changes using `tf init -upgrade` / `tf plan` / `tf apply`.

#### Per-project changes

If the missing permissions apply to just a single project, a per-project override should be used to
add the permissions needed on just that project. Unlike the global changes, this will require
setting the value of the `var.service_account_roles` variable on the `project` module for the given
project in `env-projects` to the list of project-specific permissions, instead of changing the default values within the module and triggering a
new release. Once the attribute has been added to the project module definition in `env-projects`,
manually apply the changes using `tf init -upgrade` / `tf plan` / `tf apply`.

### Caveats

- Don't use `tf destroy` on cattle unless you **really** know what you're doing. Be aware that deleting an element of an array other than the last one will result in all the resources starting from that element to be destroyed as well. So if you delete `module.something.resource[0]` Terraform will delete **everything** on that array, even if the confirmation dialog only reports the one you want to delete.
- Requesting a quota increase on GCP generally takes an hour or two for memory and CPU increases, but takes several days for increases in SSD and standard disk space.

---

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
